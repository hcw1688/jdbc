package com.hcw;


import com.hcw.entity.User;
import com.hcw.pojo.Major;
import com.hcw.utils.DatabaseUtils;
import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.util.HashMap;

/**
 * @title:DbTest
 * @desc:
 * @author:wang
 * @date:2023/9/8 9:17
 */
public class DatabaseUtilsTest {

    @Test
    public static void updateByEntityTest() {
        User user = new User();
        user.setId(5);
        user.setUserName("牛牛牛1");
        user.setEmail("635411106@qq.com");
        user.setPassword("123456");
        DatabaseUtils<User> userDao = new DatabaseUtils<User>() {};
        boolean b=userDao.updateByEntity(user);
        System.out.println(b);
    }

    @Test
    public static void selectByIdTest() {
        String id="5";
        DatabaseUtils<User> userDao = new DatabaseUtils<User>(){};
        User user=userDao.selectById(id);
        System.out.println(user.toString());
    }

    @Test
    public static void deleteByIdTest()  {
        String id="2";
        DatabaseUtils<User> userDao = new DatabaseUtils<User>() {};
        boolean b=userDao.deleteById(id);
        System.out.println(b);
    }

    /**
     * 条件列查询
     */
    @Test
    public static void listWhereColumnTest() {
        DatabaseUtils<User> userDao = new DatabaseUtils<User>() {};
        userDao.listAll("id,UserName").forEach(c->{
            System.out.print(c.getId());
            System.out.println(c.getUserName()+" ");
        });
    }

    /**
     * list列表
     */
    @Test
    public static void listTest() {
        DatabaseUtils<User> userDao = new DatabaseUtils<User>(){};
        userDao.listAll().forEach(c->{
            System.out.println(c.getId());
            System.out.println(c.getUserName());
            System.out.println(c.getEmail());
            System.out.println(c.getCreateTime());
        });
    }

    /**
     * 多条件查询
     */
    @Test
    public static void listByWhereTest() {
        DatabaseUtils<User> userDao = new DatabaseUtils<User>() {};
        HashMap<String ,String> wheres=new HashMap<>();
        wheres.put("Password","123456");
        wheres.put("UserName","牛牛牛2");
        userDao.queryByColumnNames(wheres).forEach(c->{
            System.out.println(c.getUserName());
        });
    }

    @Test
    public static void InsertTest() {
        User user = new User();
        user.setUserName("牛牛牛");
        user.setEmail("635411106@qq.com");
        user.setPassword("123456");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        user.setCreateTime(timestamp);
        DatabaseUtils<User> userDao = new DatabaseUtils<User>() {};
        boolean b = userDao.insert(user);
        System.out.println(b);
    }

    /**
     * 分页与模糊查询测试
     */
    @Test
    public static void queryByPageTest() {
        DatabaseUtils<User> userDao = new DatabaseUtils<User>() {};
        HashMap<String ,String> wheres=new HashMap<>();
        userDao.queryByPage(1,5,wheres).forEach(c->{
            System.out.println(c.getUserName());
        });
    }

    /**
     * 根据条件统计数量
     */
    @Test
    public static void queryCountTest() {
        DatabaseUtils<Major> majorDao = new DatabaseUtils<Major>() {};
        HashMap<String ,String> wheres=new HashMap<>();
        wheres.put("mname","语");
        Long count= majorDao.queryCount(wheres);
        System.out.println(count);
    }
}
