package com.hcw;

import com.google.gson.Gson;
import com.hcw.entity.User;
import com.hcw.utils.DatabaseUtils;
import com.hcw.utils.LogUtils;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;

/**
 * @title:ExceptionHandlingTest
 * @desc:异常处理试验
 * @author:wang
 * @date:2023/9/14 9:35
 */
public class ExceptionHandlingTest {

    /**
     * （思路：java核心技术卷1）
     * 空指针和算术异常测试
     * 如果出现异常希望程序继续执行的，
     *  1 可以将异常类型设为顶级类Exception，处理块catch中无需写任何代码
     *  2 正确捕获异常类型时，程序也不会中断
     */
    @Test
    public void tryCatchTest() throws IOException {
        User user=null;
        try {
          int a=1/0;//算术异常
//            user.getUserName();//空指针异常
        }catch (ArithmeticException e){

        }
        System.out.println(1);
    }

    /**
     * 在项目中日志记录
     */
    @Test
    public static void queryByPageTest() {
        DatabaseUtils<User> userDao = new DatabaseUtils<User>() {};
        HashMap<String ,String> wheres=new HashMap<>();
        userDao.queryByPage(1,5,wheres).forEach(c->{
            System.out.println(c.getUserName());
        });
    }

    /**
     * 发生异常后，日志记录信息
     * @throws IOException
     */
    @Test
    public void logException()  {
        User user=null;
        try {
            int a=1/0;//算术异常
        }catch (ArithmeticException e){
            LogUtils.error(e);
        }
    }


    @Test
    public void GsonTest(){
        System.out.println(String.valueOf(true));
        User user=new User(1,"牛牛","123456");
        Gson g=new Gson();
        String json= g.toJson(user);
        System.out.println(json);
    }

}
