package com.hcw;

import com.hcw.entity.User;
import com.hcw.utils.AnnotationUtils;
import org.testng.annotations.Test;

/**
 * @title:AnnotationTest
 * @desc:注解工具测试
 * @author:wang
 * @date:2023/9/10 16:49
 */
public class AnnotationUtilsTest {

    /**
     * 获取key字段的对象属性名
     */
    @Test
    public void getKeyPropertyNameTest(){
        String keyProperyName= AnnotationUtils.getKeyPropertyName(User.class);
        System.out.println(keyProperyName);
    }

    /**
     * 获取实体上的注解值（sql的表名）
     */
    @Test
    public void getTableNameByClassTest(){
        String tableName= AnnotationUtils.getTableName(User.class);
        System.out.println(tableName);
    }

    /**
     * 获取主键字段名，用于映射数据库的key字段
     */
    @Test
    public void getPrimaryKeyValueTest(){
        String keyName= AnnotationUtils.getPrimaryKeyValue(User.class);
        System.out.println(keyName);
    }
}
