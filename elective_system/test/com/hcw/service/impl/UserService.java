package com.hcw.service.impl;

import com.hcw.entity.User;

import java.util.List;

/**
 * @title��UserService
 * @desc:
 * @author:wang
 * @date:2023/9/9 11:31
 */
public interface UserService {

    List<User> list();
}
