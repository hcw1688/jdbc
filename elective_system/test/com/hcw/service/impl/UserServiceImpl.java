package com.hcw.service.impl;

import com.hcw.dao.UserDao;
import com.hcw.dao.impl.UserDaoImpl;
import com.hcw.entity.User;

import java.util.List;

/**
 * @title��UserServiceImpl
 * @desc:
 * @author:wang
 * @date:2023/9/9 11:32
 */
public class UserServiceImpl implements UserService {

    @Override
    public List<User> list() {
        UserDao userDao=new UserDaoImpl();
        return userDao.list();
    }
}
