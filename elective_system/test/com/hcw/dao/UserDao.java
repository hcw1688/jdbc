package com.hcw.dao;

import com.hcw.entity.User;

import java.util.List;

/**
 * @title:UserDao
 * @desc:
 * @author:wang
 * @date:2023/9/9 11:27
 */
public interface UserDao {
    List<User> list();
}
