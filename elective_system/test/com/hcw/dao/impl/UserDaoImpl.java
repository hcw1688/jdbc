package com.hcw.dao.impl;

import com.hcw.dao.UserDao;
import com.hcw.entity.User;
import com.hcw.utils.BaseDao;

import java.util.List;

/**
 * @title:UserDaoImpl
 * @desc:这是一个入门案例
 * @author:wang
 * @date:2023/9/9 11:29
 */
public class UserDaoImpl extends BaseDao<User> implements UserDao {
    @Override
    public List<User> list() {
        return this.listAll();
    }
}
