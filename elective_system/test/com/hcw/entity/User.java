package com.hcw.entity;


import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

import java.sql.Timestamp;


@Table("t_user")
public class User {
    public User(){}
    @Key(ColumnName = "Id")
    public Integer Id;

    public String UserName;
    public String Password;
    public String Email;
    public Timestamp CreateTime;

    public User(String userName) {
        UserName = userName;
    }

    public User(Integer id, String userName, String password) {
        Id = id;
        UserName = userName;
        Password = password;
    }

    public Timestamp getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Timestamp createTime) {
        CreateTime = createTime;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", UserName='" + UserName + '\'' +
                ", Password='" + Password + '\'' +
                ", Email='" + Email + '\'' +
                ", CreateTime=" + CreateTime +
                '}';
    }
}

