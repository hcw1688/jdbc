package com.hcw;

import com.hcw.entity.User;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @title:LambdaTest
 * @desc:一些lambda试验代码
 * @author:wang
 * @date:2023/9/11 15:15
 */
public class LambdaTest {

    /**
     * 过滤
     */
    @Test
    public void LambdaFilterTest() {
        List<User> list = Arrays.asList(new User("a"), new User("b"));
        Stream<User> lambda = list.stream();
        list = lambda.filter(user -> {
            return user.getUserName().equals("a")||user.getUserName().equals("b");
        }).collect(Collectors.toList());
        for (User u : list
        ) {
            System.out.println(u.getUserName());
        }
    }
}
