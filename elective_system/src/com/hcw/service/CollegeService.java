package com.hcw.service;

import com.hcw.pojo.College;

import java.util.List;

/**
 * @title:CollegeService
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public interface CollegeService {
    /**
     * 学院选择器数据
     * @return
     */
    List<College> listSelector();

    /**
     * 根据id获取数据
     * @param collegeId
     * @return
     */
    College getCollegeById(Integer collegeId);
}
