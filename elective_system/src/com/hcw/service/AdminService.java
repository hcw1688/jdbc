package com.hcw.service;

import com.hcw.pojo.Admin;

/**
 * @title:AdminService
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:41
 */
public interface AdminService {

    /**
     * 管理员登陆,不存在数据时返回null
     *
     * @param admin
     * @return
     */
    public Admin adminLogin(Admin admin);
}
