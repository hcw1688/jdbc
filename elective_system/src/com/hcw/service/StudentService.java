package com.hcw.service;

import com.hcw.pojo.Student;
import com.hcw.pojo.Student;

import java.util.List;

/**
 * @title:StudentService
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:42
 */
public interface StudentService {

    /**
     * 学生登陆校验,不存在数据时返回null
     * @param student
     * @return
     */
    public Student studentLogin(Student student);

    /**
     * 验证学生id是否存在
     * @param cid
     * @return
     */
    boolean isExistByStudentId(String cid);

    /**
     * 添加学生
     * @param course
     * @return
     */
    boolean saveStudent(Student course);

    /**
     * 获取分页数据
     * @param page
     * @param limit
     * @return
     */
    List<Student> listPage(Integer page, Integer limit);

    /**
     * 通过条件统计数据条目方法
     * @param cname
     * @return
     */
    Long countByName(String cname);

    /**
     * 通过key删除
     * @param cid
     * @return
     */
    boolean deleteStudent(Integer cid);

    /**
     * 学生编辑
     * @param course
     * @return
     */
    boolean editStudent(Student course);

    /**
     * 模糊查询数据
     * @param cname
     * @return
     */
    List<Student> listStudentByName(String cname);
}
