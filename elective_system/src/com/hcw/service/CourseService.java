package com.hcw.service;

import com.hcw.pojo.Course;

import java.util.List;

/**
 * @title:CourseService
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public interface CourseService {
    /**
     * 验证课程id是否存在
     * @param cid
     * @return
     */
     boolean isExistByCourseId(String cid);

    /**
     * 添加课程
     * @param course
     * @return
     */
     boolean saveCourse(Course course);

    /**
     * 获取分页数据
     * @param page
     * @param limit
     * @return
     */
    List<Course> listPage(Integer page, Integer limit);

    /**
     * 通过条件统计数据条目方法
     * @param cname
     * @return
     */
    Long countByName(String cname);

    /**
     * 通过key删除
     * @param cid
     * @return
     */
    boolean deleteCourse(Integer cid);

    /**
     * 课程编辑
     * @param course
     * @return
     */
    boolean editCourse(Course course);

    /**
     * 模糊查询数据
     * @param cname
     * @return
     */
    List<Course> listCourseByName(String cname);
}
