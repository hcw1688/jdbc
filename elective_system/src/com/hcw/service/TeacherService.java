package com.hcw.service;

import com.hcw.pojo.Teacher;

import java.util.HashMap;
import java.util.List;

/**
 * @title:TeacherService
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:43
 */
public interface TeacherService {

    /**
     * 教师登陆校验,不存在数据时返回null
     * @param teacher
     * @return
     */
    public Teacher teacherLogin(Teacher teacher);

    /**
     * 保存数据
     * @param teacher
     * @return
     */
     boolean teacherSave(Teacher teacher);

    /**
     * 校验教师编号是否存在
     * @param tid
     * @return
     */
     boolean isExistByTeacherId(String tid);


    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @return
     */
    List<Teacher> listPage(Integer page, Integer limit);

    /**
     * 模糊查询数据
     * @param tname
     * @return
     */
    List<Teacher> listTeacherByName(String tname);

    /**
     * 通过条件统计数据条目方法
     * @param tname
     * @return
     */
    Long countByName(String tname);

    /**
     * 通过key删除
     * @param tid
     * @return
     */
    boolean deleteTeacher(Integer tid);


    /**
     * 教师编辑
     * @param teacher
     * @return
     */
    boolean editTeacher(Teacher teacher);
}
