package com.hcw.service;

import com.hcw.dao.ClassDao;
import com.hcw.dao.MajorDao;
import com.hcw.dao.impl.ClassDaoImpl;
import com.hcw.dao.impl.MajorDaoImpl;
import com.hcw.pojo.Clazz;

import java.util.List;

/**
 * @title:ClassService
 * @desc:
 * @author:wang
 * @date:2023/9/20 16:29
 */
public interface ClassService {
     /**
      * 根据专业id获取班级数据
      * @return
      */
     List<Clazz> listByCollegeId(Integer majorId);

     /**
      * 根据班级id获取信息
      * @param majorId
      * @return
      */
     public Clazz getClazzById(Integer classId);
}
