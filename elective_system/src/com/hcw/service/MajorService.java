package com.hcw.service;

import com.hcw.pojo.Major;

import java.util.List;

/**
 * @title:MajorService
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public interface MajorService {
    /**
     * 根据学院id获取专业数据
     * @param collegeId
     * @return
     */
    List<Major> listByCollegeId(Integer collegeId);

    /**
     * 根据课程id获取课程信息
     * @param majorId
     * @return
     */
    public Major getMajorById(Integer majorId);
}
