package com.hcw.service.impl;

import com.hcw.dao.MajorDao;
import com.hcw.dao.impl.MajorDaoImpl;
import com.hcw.pojo.Major;
import com.hcw.service.MajorService;

import java.util.List;

/**
 * @title:MajorService
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class MajorServiceImpl implements MajorService {
    MajorDao majorDao = new MajorDaoImpl();

    /**
     * 根据学院id获取专业数据
     * @param collegeId
     * @return
     */
    @Override
    public List<Major> listByCollegeId(Integer collegeId) {
        return majorDao.listByCollegeId(collegeId);
    }

    /**
     * 根据课程id获取课程信息
     * @param majorId
     * @return
     */
    @Override
    public Major getMajorById(Integer majorId) {
        return  majorDao.getMajorById(majorId);
    }
}
