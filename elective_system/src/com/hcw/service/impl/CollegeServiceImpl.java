package com.hcw.service.impl;

import com.hcw.dao.CollegeDao;
import com.hcw.dao.impl.CollegeDaoImpl;
import com.hcw.pojo.College;
import com.hcw.service.CollegeService;

import java.util.List;

/**
 * @title:CollegeServiceImpl
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class CollegeServiceImpl implements CollegeService {
    CollegeDao collegeDao = new CollegeDaoImpl();

    /**
     * 学院列表数据
     * @return
     */
    @Override
    public List<College> listSelector() {
        return collegeDao.listSelector();
    }

    /**
     * 根据id获取数据
     * @param collegeId
     * @return
     */
    @Override
    public College getCollegeById(Integer collegeId) {
        return  collegeDao.getCollegeById(collegeId);
    }
}
