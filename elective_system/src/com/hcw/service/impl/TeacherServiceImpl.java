package com.hcw.service.impl;

import com.hcw.dao.TeacherDao;
import com.hcw.dao.impl.TeacherDaoImpl;
import com.hcw.pojo.Student;
import com.hcw.pojo.Teacher;
import com.hcw.service.TeacherService;

import java.util.HashMap;
import java.util.List;

/**
 * @title:TeacherServiceImpl
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:45
 */
public class TeacherServiceImpl implements TeacherService {

    TeacherDao teacherDao = new TeacherDaoImpl();

    /**
     * 教师登陆校验,不存在数据时返回null
     *
     * @param teacher
     * @return
     */
    @Override
    public Teacher teacherLogin(Teacher teacher) {
        Teacher loginTeacher = teacherDao.teacherLogin(teacher);
        return loginTeacher;
    }

    /**
     * 保存数据
     *
     * @param teacher
     * @return
     */
    @Override
    public boolean teacherSave(Teacher teacher) {
        return teacherDao.teacherSave(teacher);
    }

    /**
     * 校验教师编号是否存在
     *
     * @param tid
     * @return
     */
    @Override
    public boolean isExistByTeacherId(String tid) {
        return teacherDao.isExistByTeacherId(tid);
    }

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @return
     */
    @Override
    public List<Teacher> listPage(Integer page, Integer limit) {
        return teacherDao.listPage(page, limit, new HashMap<>());
    }

    /**
     * 模糊查询数据
     *
     * @param tname
     * @return
     */
    @Override
    public List<Teacher> listTeacherByName(String tname) {
        HashMap<String, String> where = new HashMap<>();
        where.put("tname", tname);
        return teacherDao.listPage(0, 0, where);
    }

    /**
     * 通过条件统计数据条目方法
     *
     * @param tname
     * @return
     */
    @Override
    public Long countByName(String tname) {
        HashMap<String, String> where = new HashMap<>();
        where.put("tname", tname);
        return teacherDao.countByName(where);
    }

    /**
     * 通过key删除
     *
     * @param tid
     * @return
     */
    @Override
    public boolean deleteTeacher(Integer tid) {
        return teacherDao.deleteTeacher(+tid);
    }

    /**
     * 教师编辑
     *
     * @param teacher
     * @return
     */
    @Override
    public boolean editTeacher(Teacher teacher) {
        return teacherDao.editTeacher(teacher);
    }
}
