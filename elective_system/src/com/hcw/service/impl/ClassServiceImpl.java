package com.hcw.service.impl;

import com.hcw.dao.ClassDao;
import com.hcw.dao.MajorDao;
import com.hcw.dao.impl.ClassDaoImpl;
import com.hcw.dao.impl.MajorDaoImpl;
import com.hcw.pojo.Clazz;
import com.hcw.pojo.Major;
import com.hcw.service.ClassService;
import com.hcw.service.MajorService;

import java.util.HashMap;
import java.util.List;

/**
 * @title:MajorService
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class ClassServiceImpl implements ClassService {
    ClassDao classDao = new ClassDaoImpl();
    /**
     * 根据专业id获取班级数据（选择器数据）
     * @param majorId
     * @return
     */
    @Override
    public List<Clazz> listByCollegeId(Integer majorId) {
        HashMap<String,String> wheres=new HashMap<>();
        wheres.put("majorid",String.valueOf(majorId));
        return classDao.listByCollegeId(wheres);
    }


    /**
     * 根据班级id获取信息
     * @param classId
     * @return
     */
    @Override
    public Clazz getClazzById(Integer classId) {
        return  classDao.getClazzById(classId);
    }
}
