package com.hcw.service.impl;

import com.hcw.dao.CourseDao;
import com.hcw.dao.impl.CourseDaoImpl;
import com.hcw.pojo.Course;
import com.hcw.service.CourseService;

import java.util.HashMap;
import java.util.List;

/**
 * @title:CourseServiceImpl
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class CourseServiceImpl implements CourseService {
    CourseDao courseDao = new CourseDaoImpl();
    /**
     * 验证课程id是否存在
     * @param cid
     * @return
     */
    @Override
    public boolean isExistByCourseId(String cid) {
        return courseDao.isExistByCourseId(cid);
    }

    /**
     * 添加课程
     * @param course
     * @return
     */
    public boolean saveCourse(Course course) {
        return courseDao.saveCourse(course);
    }

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @return
     */
    @Override
    public List<Course> listPage(Integer page, Integer limit){
        return courseDao.listPage(page,limit,new HashMap<>());
    }

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    @Override
    public Long countByName(String cname) {
        HashMap<String, String> wheres=new HashMap<>();
        wheres.put("cname",cname);
        return courseDao.countByName(wheres);
    }

    /**
     * 通过key删除
     * @param cid
     * @return
     */
    @Override
    public boolean deleteCourse(Integer cid) {
        return courseDao.deleteCourse(cid);
    }

    @Override
    public boolean editCourse(Course course) {
        return courseDao.editCourse(course);
    }

    @Override
    public List<Course> listCourseByName(String cname) {
        HashMap<String,String> where=new HashMap<>();
        where.put("cname",cname);
        return courseDao.listCourseByName(where);
    }
}
