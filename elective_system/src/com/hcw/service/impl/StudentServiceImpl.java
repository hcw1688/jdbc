package com.hcw.service.impl;

import com.hcw.dao.StudentDao;
import com.hcw.dao.impl.StudentDaoImpl;
import com.hcw.pojo.Student;
import com.hcw.service.StudentService;

import java.util.HashMap;
import java.util.List;

/**
 * @title:StudentServiceImpl
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:44
 */
public class StudentServiceImpl implements StudentService {
    StudentDao studentDao=new StudentDaoImpl();

    /**
     * 学生登陆校验,不存在数据时返回null
     * @param student
     * @return
     */
    @Override
    public Student studentLogin(Student student) {
        Student loginStudent = studentDao.studentLogin(student);
        return loginStudent;
    }

    /**
     * 验证课程id是否存在
     * @param sid
     * @return
     */
    @Override
    public boolean isExistByStudentId(String sid) {
        return studentDao.isExistByStudentId(sid);
    }

    /**
     * 添加课程
     * @param student
     * @return
     */
    public boolean saveStudent(Student student) {
        return studentDao.saveStudent(student);
    }

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @return
     */
    @Override
    public List<Student> listPage(Integer page, Integer limit){
        return studentDao.listPage(page,limit,new HashMap<>());
    }

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    @Override
    public Long countByName(String sname) {
        HashMap<String, String> wheres=new HashMap<>();
        wheres.put("sname",sname);
        return studentDao.countByName(wheres);
    }

    /**
     * 通过key删除
     * @param sid
     * @return
     */
    @Override
    public boolean deleteStudent(Integer sid) {
        return studentDao.deleteStudent(sid);
    }

    @Override
    public boolean editStudent(Student student) {
        return studentDao.editStudent(student);
    }

    @Override
    public List<Student> listStudentByName(String sname) {
        HashMap<String,String> where=new HashMap<>();
        where.put("sname",sname);
        return studentDao.listStudentByName(where);
    }
}
