package com.hcw.service.impl;

import com.hcw.dao.AdminDao;
import com.hcw.dao.impl.AdminDaoImpl;
import com.hcw.pojo.Admin;
import com.hcw.service.AdminService;

/**
 * @title:AdminServiceImpl
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:43
 */
public class AdminServiceImpl implements AdminService {
    AdminDao adminDao = new AdminDaoImpl();

    /**
     * 管理员登陆,不存在数据时返回null
     *
     * @param admin
     * @return
     */
    @Override
    public Admin adminLogin(Admin admin) {
        Admin loginAdmin = adminDao.adminLogin(admin);
        return loginAdmin;
    }

}
