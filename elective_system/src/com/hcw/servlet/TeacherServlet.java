package com.hcw.servlet;

import com.google.gson.Gson;
import com.hcw.pojo.*;
import com.hcw.service.TeacherService;
import com.hcw.service.impl.TeacherServiceImpl;
import com.ramostear.captcha.HappyCaptcha;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * @title:TeacherServlet
 * @desc:
 * @author:wang
 * @date:2023/9/9 18:56
 */
@WebServlet("/teacherServlet")
public class TeacherServlet extends HttpServlet {
    TeacherService teacherService = new TeacherServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        resp.setContentType("text/html;charset=utf-8");
        req.setCharacterEncoding("UTF-8");
        String method = req.getParameter("flag");

        if ("login".equals(method)) {
            teacherLogin(req, resp);
        } else if ("logout".equals(method)) {
            teacherLogout(req, resp);
        } else if ("add".equals(method)) {
            addTeacher(req, resp);
        }else if ("checktid".equals(method)) {
            isExistByTeacherId(req, resp);
        } else if ("add".equals(method)) {
            addTeacher(req, resp);
        } else if ("delete".equals(method)) {
            deleTeacher(req, resp);
        } else if ("edit".equals(method)) {
            editTeacher(req, resp);
        }
    }


    /**
     * 教师编辑
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void editTeacher(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tid = req.getParameter("tid");
        String tname = req.getParameter("tname");
        String tsex = req.getParameter("tsex");
        String tpassword = req.getParameter("tpassword");
        String introduction = req.getParameter("introduction");
        //密码默认为123456
        Teacher course = new Teacher(tid, tname, tpassword, tsex, introduction);
        boolean isTrue = teacherService.editTeacher(course);
        resp.getWriter().write(String.valueOf(isTrue));
    }


    /**
     * 删除教师
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void deleTeacher(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tid = req.getParameter("tid");
        boolean isSuccess = teacherService.deleteTeacher(Integer.parseInt(tid));
        resp.getWriter().write(String.valueOf(isSuccess));
    }


    public void isExistByTeacherId(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tid = req.getParameter("tid");
        boolean isExists=teacherService.isExistByTeacherId(tid);
        resp.getWriter().write(String.valueOf(isExists));
    }

    private void addTeacher(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tid = req.getParameter("tid");
        String tname = req.getParameter("tname");
        String tpassword = req.getParameter("tpassword");
        String tsex = req.getParameter("tsex");
        String introduction = req.getParameter("introduction");
        Teacher teacher = new Teacher(tid, tname, tpassword, tsex, introduction);
        boolean isTrue = teacherService.teacherSave(teacher);
        resp.getWriter().write(String.valueOf(isTrue));
//        if (isTrue) {
//            showSuccessMessageAndRedirect(req, resp, "添加成功");
//        } else {
//            showSuccessMessageAndRedirect(req, resp, "操作失败!");
//        }
    }

    /**
     * 管理员登陆
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    private void teacherLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String aname = req.getParameter("aname");
        String apassword = req.getParameter("apassword");
        String code = req.getParameter("code");
        Teacher teacher = new Teacher(aname, apassword);
//         验证用户
        Teacher loginTeacher = teacherService.teacherLogin(teacher);
//         验证码校验
        boolean isCaptchaValid = HappyCaptcha.verification(req, code, true);
        if (isCaptchaValid == false) {
            req.setAttribute("msg", "验证码错误,请重新输入！");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
            return;
        }
//       登陆成功，重定向首页
        if (loginTeacher != null && isCaptchaValid) {
            req.getSession().setAttribute("tname", loginTeacher.getTname());
            resp.sendRedirect(req.getContextPath() + "/teacher/teacherFace.jsp");
        } else {
            req.setAttribute("msg", "用户名或者密码错误");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }

    private void teacherLogout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //重定向到登录页面
        req.getSession().invalidate();
        req.setAttribute("msg", null);
        resp.sendRedirect(req.getContextPath() + "/login.jsp");
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
        String method = req.getParameter("flag");
        if ("checkTid".equals(method)) {
            isExistByTeacherId(req, resp);
        } else if ("pageList".equals(method)) {
            pageList(req, resp);
        } else if ("search".equals(method)) {
            searchList(req, resp);
        }
    }


    /**
     * 分页数据
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    public void pageList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String page = req.getParameter("page");
        String limit = req.getParameter("limit");
        List<Teacher> list = teacherService.listPage(Integer.parseInt(page), Integer.parseInt(limit));
        Long count = teacherService.countByName(null);
        RespDTO respDto = RespDTO.ok("查询成功！", list, count);
        Gson gson = new Gson();
        resp.getWriter().write(gson.toJson(respDto));
    }

    /**
     * 分页数据查询
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    public void searchList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tname = req.getParameter("tname");
        List<Teacher> list = teacherService.listTeacherByName(tname);
        RespDTO respDto = RespDTO.ok("查询成功！", list, teacherService.countByName(tname));
        Gson gson = new Gson();
        resp.getWriter().write(gson.toJson(respDto));
    }


    /**
     * 操作状态消息提示（跳转到管理员首页）
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private static void showSuccessMessageAndRedirect(HttpServletRequest req, HttpServletResponse resp, String msg) throws IOException {
        // 设置延时时间（单位:毫秒）
        int delay = 0; // 延时1秒
        PrintWriter writer = resp.getWriter();
        writer.println("<script>");
        writer.println("window.onload = function() {");
        writer.println("alert('" + msg + "');");
        writer.println("setTimeout(function() {");
        writer.println("window.location.href ='/admin/adminFace.jsp;'");
        writer.println("}, " + delay + ");");
        writer.println("}");
        writer.println("</script>");
    }
}
