package com.hcw.servlet;

import com.google.gson.Gson;
import com.hcw.pojo.College;
import com.hcw.pojo.Course;
import com.hcw.pojo.Major;
import com.hcw.pojo.RespDTO;
import com.hcw.service.CollegeService;
import com.hcw.service.CourseService;
import com.hcw.service.MajorService;
import com.hcw.service.impl.CollegeServiceImpl;
import com.hcw.service.impl.CourseServiceImpl;
import com.hcw.service.impl.MajorServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @title:CourseServlet
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:39
 */
@WebServlet("/courseServlet")
public class CourseServlet extends HttpServlet {
    CourseService courseService = new CourseServiceImpl();
    CollegeService collegeService = new CollegeServiceImpl();
    MajorService majorService = new MajorServiceImpl();

    /**
     * 数据查询操作（校验和分页列表）
     * @param req
     * @param resp
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        resp.setContentType("text/html;charset=utf-8");
        req.setCharacterEncoding("UTF-8");
        String method = req.getParameter("flag");
        if ("chexckCid".equals(method)) {
            isExistByTeacherId(req, resp);
        } else if ("pageList".equals(method)) {
            pageList(req, resp);
        } else if ("search".equals(method)) {
            searchList(req, resp);
        }
    }

    /**
     * 分页数据
     * @param req
     * @param resp
     * @throws IOException
     */
    public void pageList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String page = req.getParameter("page");
        String limit = req.getParameter("limit");
        List<Course> list = courseService.listPage(Integer.parseInt(page), Integer.parseInt(limit));
        Long count=courseService.countByName(null);
        RespDTO respDto = RespDTO.ok("查询成功！", list,count);
        Gson gson = new Gson();
        resp.getWriter().write(gson.toJson(respDto));
    }

    /**
     * 分页数据查询
     * @param req
     * @param resp
     * @throws IOException
     */
    public void searchList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String cname = req.getParameter("cname");
        List<Course> list = courseService.listCourseByName(cname);
        RespDTO respDto = RespDTO.ok("查询成功！", list, courseService.countByName(cname));
        Gson gson = new Gson();
        resp.getWriter().write(gson.toJson(respDto));
    }

    /**
     * 校验教师编号是否存在
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    public void isExistByTeacherId(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String cid = req.getParameter("cid");
        boolean isExists = courseService.isExistByCourseId(cid);
        resp.getWriter().write(String.valueOf(isExists));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        req.setCharacterEncoding("UTF-8");
        String method = req.getParameter("flag");
        if ("add".equals(method)) {
            addCourse(req, resp);
        } else if ("delete".equals(method)) {
            deleCourse(req, resp);
        }else if ("edit".equals(method)) {
            editCourse(req, resp);
        }
    }

    /**
     * 添加课程
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void addCourse(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String cid = req.getParameter("cid");
        String cname = req.getParameter("cname");
        String cintroduction = req.getParameter("cintroduction");
        String type = req.getParameter("type");
        String c_id = req.getParameter("belongcoll");
        String m_id = req.getParameter("belongpro");

//根据院系编号查询院系信息
        College college = collegeService.getCollegeById(Integer.parseInt(c_id));
        String title = college.getTitle();
////根据专业编号查询专业信息
        Major major = majorService.getMajorById(Integer.parseInt(m_id));
        String mname = major.getMname();

        Course course = new Course(cid, cname, cintroduction, type, title, mname);
        boolean isTrue = courseService.saveCourse(course);
        resp.getWriter().write(String.valueOf(isTrue));
    }

    /**
     * 课程编辑
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void editCourse(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String cid = req.getParameter("cid");
        String cname = req.getParameter("cname");
        String cintroduction = req.getParameter("cintroduction");
        String type = req.getParameter("type");
        String c_id = req.getParameter("belongcoll");
        String m_id = req.getParameter("belongpro");

//根据院系编号查询院系信息
        College college = collegeService.getCollegeById(Integer.parseInt(c_id));
        String title = college.getTitle();
////根据专业编号查询专业信息
        Major major = majorService.getMajorById(Integer.parseInt(m_id));
        String mname = major.getMname();

        Course course = new Course(cid, cname, cintroduction, type, title, mname);
        boolean isTrue = courseService.editCourse(course);
        resp.getWriter().write(String.valueOf(isTrue));
    }

    /**
     * 删除课程
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void deleCourse(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String cid = req.getParameter("cid");
        boolean isSuccess = courseService.deleteCourse(Integer.parseInt(cid));
        resp.getWriter().write(String.valueOf(isSuccess));
    }

    /**
     * 操作状态消息提示（跳转到管理员首页）
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private static void showSuccessMessageAndRedirect(HttpServletRequest req, HttpServletResponse resp, String msg) throws IOException {
        // 设置延时时间（单位:毫秒）
        int delay = 0; // 延时1秒
        PrintWriter writer = resp.getWriter();
        writer.println("<script>");
        writer.println("window.onload = function() {");
        writer.println("alert('" + msg + "');");
        writer.println("setTimeout(function() {");
        writer.println("window.location.href ='/admin/adminFace.jsp;'");
        writer.println("}, " + delay + ");");
        writer.println("}");
        writer.println("</script>");
    }
}
