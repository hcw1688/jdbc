package com.hcw.servlet;

import com.hcw.pojo.Admin;
import com.hcw.service.AdminService;
import com.hcw.service.impl.AdminServiceImpl;
import com.ramostear.captcha.HappyCaptcha;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
/**
 * @title:AdminServlet
 * @desc:
 * @author:wang
 * @date:2023/9/9 18:52
 */
@WebServlet("/adminServlet")
public class AdminServlet extends HttpServlet {
    AdminService adminService = new AdminServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        resp.setContentType("text/html;charset=utf-8");
        String method = req.getParameter("flag");

        if ("login".equals(method)) {
            adminLogin(req, resp);
        } else if ("logout".equals(method)) {
            adminLogout(req, resp);
        }
    }



    /**
     * 管理员登陆
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    private void adminLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String aname = req.getParameter("aname");
        String apassword = req.getParameter("apassword");
        String code = req.getParameter("code");

        Admin admin = new Admin(aname, apassword);

//         验证用户
        Admin loginAdmin = adminService.adminLogin(admin);

//         验证码校验
        boolean isCaptchaValid = HappyCaptcha.verification(req, code, true);

        if (isCaptchaValid == false) {
            req.setAttribute("msg", "验证码错误,请重新输入！");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
            return;
        }

//       登陆成功，重定向首页
        if (loginAdmin != null && isCaptchaValid) {
            req.getSession().setAttribute("aname", loginAdmin.getAname());
            resp.sendRedirect(req.getContextPath() + "/admin/adminFace.jsp");
        } else {
            req.setAttribute("msg", "用户名或者密码错误");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }

    /**
     * 退出登陆
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    private void adminLogout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //重定向到登录页面
        req.getSession().invalidate();
        req.setAttribute("msg", null);
        resp.sendRedirect(req.getContextPath() + "/login.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
}
