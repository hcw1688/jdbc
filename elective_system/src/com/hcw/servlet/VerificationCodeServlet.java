package com.hcw.servlet;

import com.ramostear.captcha.HappyCaptcha;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @title:VerificationCode
 * @desc:开源链接-->https://gitee.com/ramostear/Happy-Captcha
 * @author:wang
 * @date:2023/9/11 16:02
 */
@WebServlet("/code")
public class VerificationCodeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HappyCaptcha.require(req,resp).build().finish();
    }
}
