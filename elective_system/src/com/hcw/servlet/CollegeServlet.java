package com.hcw.servlet;

import com.google.gson.Gson;
import com.hcw.pojo.College;
import com.hcw.service.CollegeService;
import com.hcw.service.impl.CollegeServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @title:CollegeServlet
 * @desc:
 * @author:wang
 * @date:2023/9/14 15:42
 */
@WebServlet("/collegeServlet")
public class CollegeServlet extends HttpServlet {

    CollegeService collegeService = new CollegeServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf-8");
        String method = req.getParameter("flag");
        if ("getColleges".equals(method)) {
            getCollegeList(req, resp);
        }
    }

    /**
     * 学院下拉选项数据
     * @param req
     * @param resp
     * @throws IOException
     */
    public void getCollegeList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Gson json = new Gson();
        List<College> colleges = collegeService.listSelector();
        String result = json.toJson(colleges);
        resp.getWriter().write(result);
    }
}
