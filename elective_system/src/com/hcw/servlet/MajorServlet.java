package com.hcw.servlet;

import com.google.gson.Gson;
import com.hcw.pojo.Major;
import com.hcw.service.MajorService;
import com.hcw.service.impl.MajorServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @title:MajorServlet
 * @desc:
 * @author:wang
 * @date:2023/9/14 15:42
 */
@WebServlet("/majorServlet")
public class MajorServlet extends HttpServlet {

    MajorService majorService = new MajorServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf-8");
        String method = req.getParameter("flag");
        if ("getMajors".equals(method)) {
            getMajorList(req, resp);
        }
    }

    /**
     * 根据学院id获取专业数据列表
     * @param req
     * @param resp
     * @throws IOException
     */
    public void getMajorList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Gson json = new Gson();
        String collegeId = req.getParameter("collegeId");
        List<Major> colleges = majorService.listByCollegeId(Integer.valueOf(collegeId));
        String result = json.toJson(colleges);
        resp.getWriter().write(result);
    }
}
