package com.hcw.servlet;

import com.google.gson.Gson;
import com.hcw.pojo.*;
import com.hcw.pojo.Student;
import com.hcw.service.ClassService;
import com.hcw.service.CollegeService;
import com.hcw.service.MajorService;
import com.hcw.service.StudentService;
import com.hcw.service.impl.ClassServiceImpl;
import com.hcw.service.impl.CollegeServiceImpl;
import com.hcw.service.impl.MajorServiceImpl;
import com.hcw.service.impl.StudentServiceImpl;
import com.ramostear.captcha.HappyCaptcha;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @title:StudentServlet
 * @desc:
 * @author:wang
 * @date:2023/9/9 18:56
 */
@WebServlet("/studentServlet")
public class StudentServlet extends HttpServlet {
    StudentService studentService = new StudentServiceImpl();
    CollegeService collegeService = new CollegeServiceImpl();
    MajorService majorService = new MajorServiceImpl();

    ClassService classService = new ClassServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        resp.setContentType("text/html;charset=utf-8");
        String method = req.getParameter("flag");
        if ("login".equals(method)) {
            studentLogin(req, resp);
        } else if ("logout".equals(method)) {
            studentLogout(req, resp);
        } else if ("add".equals(method)) {
            addStudent(req, resp);
        } else if ("delete".equals(method)) {
            deleStudent(req, resp);
        } else if ("edit".equals(method)) {
            editStudent(req, resp);
        }
    }


    /**
     * 添加学生
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void addStudent(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String sid = req.getParameter("sid");
        String sidcard = req.getParameter("sidcard");
        String sname = req.getParameter("sname");
        String ssex = req.getParameter("ssex");
        String sage = req.getParameter("sage");
        String spassword = req.getParameter("spassword");
        String belongcollId = req.getParameter("belongcoll");
        String belongproId = req.getParameter("belongpro");
        String classrId = req.getParameter("classr");
        //根据院系编号查询
        College college = collegeService.getCollegeById(Integer.parseInt(belongcollId));
        String title = college.getTitle();
        //根据专业编号查询
        Major major = majorService.getMajorById(Integer.parseInt(belongproId));
        String mname = major.getMname();
        ////根据班级编号查询
        Clazz clazz = classService.getClazzById(Integer.parseInt(classrId));
        String className = clazz.getClassName();
//        初始密码为123456
        Student course = new Student(sid, sname, sidcard, ssex, spassword, sage, className, mname, title);
        boolean isTrue = studentService.saveStudent(course);
        resp.getWriter().write(String.valueOf(isTrue));
    }


    /**
     * 学生编辑
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void editStudent(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String sid = req.getParameter("sid");
        String sidcard = req.getParameter("sidcard");
        String sname = req.getParameter("sname");
        String ssex = req.getParameter("ssex");
        String sage = req.getParameter("sage");
        String spassword = req.getParameter("spassword");
        String belongcollId = req.getParameter("belongcoll");
        String belongproId = req.getParameter("belongpro");
        String classrId = req.getParameter("classr");
        //根据院系编号查询
        College college = collegeService.getCollegeById(Integer.parseInt(belongcollId));
        String title = college.getTitle();
        //根据专业编号查询
        Major major = majorService.getMajorById(Integer.parseInt(belongproId));
        String mname = major.getMname();
        ////根据班级编号查询
        Clazz clazz = classService.getClazzById(Integer.parseInt(classrId));
        String className = clazz.getClassName();
//      初始密码为123456
        Student course = new Student(sid, sname, sidcard, ssex, spassword, sage, className, mname, title);
        boolean isTrue = studentService.editStudent(course);
        resp.getWriter().write(String.valueOf(isTrue));
    }


    /**
     * 删除学生
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    private void deleStudent(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String sid = req.getParameter("sid");
        boolean isSuccess = studentService.deleteStudent(Integer.parseInt(sid));
        resp.getWriter().write(String.valueOf(isSuccess));
    }


    /**
     * 管理员登陆
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    private void studentLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String aname = req.getParameter("aname");
        String apassword = req.getParameter("apassword");
        String code = req.getParameter("code");

        Student student = new Student(aname, apassword);

//         验证用户
        Student loginStudent = studentService.studentLogin(student);

//         验证码校验
        boolean isCaptchaValid = HappyCaptcha.verification(req, code, true);

        if (isCaptchaValid == false) {
            req.setAttribute("msg", "验证码错误,请重新输入！");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
            return;
        }

//       登陆成功，重定向首页
        if (loginStudent != null && isCaptchaValid) {
            req.getSession().setAttribute("sname", loginStudent.getSname());
            resp.sendRedirect(req.getContextPath() + "/student/studentFace.jsp");
        } else {
            req.setAttribute("msg", "用户名或者密码错误");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }

    private void studentLogout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //重定向到登录页面
        req.getSession().invalidate();
        req.setAttribute("msg", null);
        resp.sendRedirect(req.getContextPath() + "/login.jsp");
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        req.setCharacterEncoding("UTF-8");
        String method = req.getParameter("flag");
        if ("chexckSid".equals(method)) {
            isExistByTeacherId(req, resp);
        } else if ("pageList".equals(method)) {
            pageList(req, resp);
        } else if ("search".equals(method)) {
            searchList(req, resp);
        }
    }

    /**
     * 分页数据
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    public void pageList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String page = req.getParameter("page");
        String limit = req.getParameter("limit");
        List<Student> list = studentService.listPage(Integer.parseInt(page), Integer.parseInt(limit));
        Long count = studentService.countByName(null);
        RespDTO respDto = RespDTO.ok("查询成功！", list, count);
        Gson gson = new Gson();
        resp.getWriter().write(gson.toJson(respDto));
    }

    /**
     * 分页数据查询
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    public void searchList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String sname = req.getParameter("sname");
        List<Student> list = studentService.listStudentByName(sname);
        RespDTO respDto = RespDTO.ok("查询成功！", list, studentService.countByName(sname));
        Gson gson = new Gson();
        resp.getWriter().write(gson.toJson(respDto));
    }

    /**
     * 校验学生编号是否存在
     *
     * @param req
     * @param resp
     * @throws IOException
     */
    public void isExistByTeacherId(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String sid = req.getParameter("sid");
        boolean isExists = studentService.isExistByStudentId(sid);
        resp.getWriter().write(String.valueOf(isExists));
    }


}
