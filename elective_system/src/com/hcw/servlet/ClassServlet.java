package com.hcw.servlet;

import com.google.gson.Gson;
import com.hcw.pojo.Clazz;
import com.hcw.service.ClassService;
import com.hcw.service.impl.ClassServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @title:ClazzServlet
 * @desc:
 * @author:wang
 * @date:2023/9/14 15:42
 */
@WebServlet("/classServlet")
public class ClassServlet extends HttpServlet {

    ClassService classService = new ClassServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf-8");
        String method = req.getParameter("flag");
        if("getClasss".equals(method)) {
            getClazzList(req, resp);
        }
    }

    /**
     * 根据专业id获取班级数据列表
     * @param req
     * @param resp
     * @throws IOException
     */
    public void getClazzList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Gson json = new Gson();
        String majorId = req.getParameter("majorId");
        List<Clazz> listClass = classService.listByCollegeId(Integer.valueOf(majorId));
        String result = json.toJson(listClass);
        resp.getWriter().write(result);
    }
}
