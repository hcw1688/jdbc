package com.hcw.dao;

import com.hcw.pojo.Clazz;

import java.util.HashMap;
import java.util.List;

/**
 * @title:ClassDaoImpl
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public interface ClassDao {

     /**
      * 根据专业id获取班级数据
      * @param majorId
      * @return
      */
     List<Clazz> listByCollegeId(HashMap<String, String> wheres);

     /**
      * 根据班级id获取信息
      * @param classId
      * @return
      */
     public Clazz getClazzById(Integer classId);
}
