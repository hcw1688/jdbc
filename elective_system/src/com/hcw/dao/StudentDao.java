package com.hcw.dao;

import com.hcw.pojo.Student;

import java.util.HashMap;
import java.util.List;

/**
 * @title:StudentDao
 * @desc:
 * @author:wang
 * @date:2023/9/9 11:23
 */
public interface StudentDao {
    /**
     * 学生登陆校验，不存在数据时返回null
     *
     * @param student
     * @return
     */
    public Student studentLogin(Student student);

    /**
     * 验证学生id是否存在
     *
     * @param cid
     * @return
     */
    boolean isExistByStudentId(String cid);

    /**
     * 添加学生
     * @param course
     * @return
     */
    boolean saveStudent(Student course);

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @param name 模糊查询条件
     * @return
     */
    List<Student> listPage(Integer page, Integer limit, HashMap<String,String> wheres);

    /**
     * 模糊查询数据
     * @param wheres
     * @return
     */
    List<Student> listStudentByName(HashMap<String,String> wheres);

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    Long countByName(HashMap<String,String> wheres);

    /**
     * 通过key删除
     * @param cid
     * @return
     */
    boolean deleteStudent(Integer cid);


    /**
     * 课程编辑
     * @param course
     * @return
     */
    boolean editStudent(Student course);

}
