package com.hcw.dao;

import com.hcw.pojo.Major;

import java.util.List;

/**
 * @title:MajorDao
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public interface MajorDao {

     /**
      * 根据学院id获取专业数据
      * @return
      */
     List<Major> listByCollegeId(Integer collegeId);

     /**
      * 根据课程id获取课程信息
      * @param majorId
      * @return
      */
     public Major getMajorById(Integer majorId);
}
