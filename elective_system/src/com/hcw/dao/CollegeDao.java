package com.hcw.dao;

import com.hcw.pojo.College;

import java.util.List;

/**
 * @title:TeacherDao
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public interface CollegeDao {

    /**
     * 全部学院数据
     * @return
     */
    List<College> listSelector();

    /**
     * 根据id获取数据
     * @param collegeId
     * @return
     */
    College getCollegeById(Integer collegeId);
}
