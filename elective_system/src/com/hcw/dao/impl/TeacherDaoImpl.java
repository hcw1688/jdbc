package com.hcw.dao.impl;

import com.hcw.dao.TeacherDao;
import com.hcw.pojo.Teacher;
import com.hcw.pojo.Teacher;
import com.hcw.utils.BaseDao;

import java.util.HashMap;
import java.util.List;

/**
 * @title:TeacherDaoImpl
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:39
 */
public class TeacherDaoImpl extends BaseDao<Teacher> implements TeacherDao {

    /**
     * 教师登陆校验，不存在数据时返回null
     *
     * @param teacher
     * @return
     */
    @Override
    public Teacher teacherLogin(Teacher teacher) {
        HashMap<String, String> map = new HashMap<>();
        map.put("tid", teacher.getTid());
        map.put("tpassword", teacher.getTpassword());
        List<Teacher> list = this.queryByColumnNames(map);
        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    /**
     * 添加教师
     * @param teacher
     * @return
     */
    @Override
    public boolean teacherSave(Teacher teacher) {
        return this.insert(teacher);
    }

    /**
     * 校验教师是否存在
     * @param tid
     * @return
     */
    @Override
    public boolean isExistByTeacherId(String tid) {
        Teacher teacher = this.selectById(tid);
        return teacher != null ? true : false;
    }

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @return
     */
    @Override
    public List<Teacher> listPage(Integer page, Integer limit, HashMap<String,String> wheres){
        return this.queryByPage(page,limit,wheres);
    }

    /**
     * 模糊查询数据
     * @param wheres
     * @return
     */
    @Override
    public List<Teacher> listTeacherByName(HashMap<String, String> wheres) {
//        搜索无需进行分页
        return this.listPage(0,0,wheres);
    }

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    @Override
    public Long countByName(HashMap<String, String> wheres) {
        return this.queryCount(wheres);
    }

    /**
     * 通过key删除
     * @param tid
     * @return
     */
    @Override
    public boolean deleteTeacher(Integer tid) {
        return this.deleteById(String.valueOf(tid));
    }

    /**
     * 教师编辑
     * @param teacher
     * @return
     */
    @Override
    public boolean editTeacher(Teacher teacher) {
        return this.updateByEntity(teacher);
    }
}
