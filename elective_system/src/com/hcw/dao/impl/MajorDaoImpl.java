package com.hcw.dao.impl;

import com.hcw.dao.MajorDao;
import com.hcw.pojo.Major;
import com.hcw.utils.BaseDao;

import java.util.HashMap;
import java.util.List;

/**
 * @title:MajorDaoImpl
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class MajorDaoImpl extends BaseDao<Major> implements MajorDao {

    /**
     * 根据学院id获取专业数据
     * @return
     */
    @Override
    public List<Major> listByCollegeId(Integer collegeId) {
        HashMap<String,String> wheres=new HashMap<>();
        wheres.put("collegeid",String.valueOf(collegeId));
        return this.queryByColumnNames(wheres);
    }


    /**
     * 根据课程id获取课程信息
     * @param majorId
     * @return
     */
    @Override
    public Major getMajorById(Integer majorId) {
        return  this.selectById(String.valueOf(majorId));
    }
}
