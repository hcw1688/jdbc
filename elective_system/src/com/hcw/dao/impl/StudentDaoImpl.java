package com.hcw.dao.impl;

import com.hcw.dao.StudentDao;
import com.hcw.pojo.Student;
import com.hcw.pojo.Student;
import com.hcw.utils.BaseDao;

import java.util.HashMap;
import java.util.List;

/**
 * @title:StudentDaoImpl
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:36
 */
public class StudentDaoImpl extends BaseDao<Student> implements StudentDao {

    /**
     * 学生登陆校验，不存在数据时返回null
     *
     * @param student
     * @return
     */
    @Override
    public Student studentLogin(Student student) {
        HashMap<String, String> map = new HashMap<>();
        map.put("sid", student.getSid());
        map.put("spassword", student.getSpassword());
        List<Student> list = this.queryByColumnNames(map);
        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    /**
     * 验证课程id是否存在
     * @param cid
     * @return
     */
    @Override
    public boolean isExistByStudentId(String cid) {
        Student student= this.selectById(cid);
        return student!=null?true:false;
    }

    /**
     * 添加课程
     * @param student
     * @return
     */
    @Override
    public boolean saveStudent(Student student) {
        return this.insert(student);
    }

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @param name 模糊查询条件
     * @return
     */
    @Override
    public List<Student> listPage(Integer page, Integer limit, HashMap<String,String> wheres){
        return this.queryByPage(page,limit,wheres);
    }

    /**
     * 模糊查询数据
     * @param wheres
     * @return
     */
    @Override
    public List<Student> listStudentByName(HashMap<String, String> wheres) {
//        搜索无需进行分页
        return this.listPage(0,0,wheres);
    }

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    @Override
    public Long countByName(HashMap<String, String> wheres) {
        return this.queryCount(wheres);
    }

    /**
     * 通过key删除
     * @param cid
     * @return
     */
    @Override
    public boolean deleteStudent(Integer cid) {
        return this.deleteById(String.valueOf(cid));
    }

    /**
     * 编辑
     * @param student
     * @return
     */
    @Override
    public boolean editStudent(Student student) {
        return this.updateByEntity(student);
    }
}
