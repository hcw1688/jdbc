package com.hcw.dao.impl;

import com.hcw.dao.ClassDao;
import com.hcw.pojo.Clazz;
import com.hcw.utils.BaseDao;

import java.util.HashMap;
import java.util.List;

/**
 * @title:ClassDaoImpl
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class ClassDaoImpl extends BaseDao<Clazz> implements ClassDao {

    /**
     * 根据专业id获取班级数据（选择器数据）
     * @param majorId
     * @return
     */
    @Override
    public List<Clazz> listByCollegeId(HashMap<String, String> wheres) {
        return this.queryByColumnNames(wheres);
    }

    /**
     * 根据班级id获取信息
     * @param classId
     * @return
     */
    @Override
    public Clazz getClazzById(Integer classId) {
        return  this.selectById(String.valueOf(classId));
    }
}
