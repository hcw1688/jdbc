package com.hcw.dao.impl;

import com.hcw.dao.CourseDao;
import com.hcw.pojo.Course;
import com.hcw.utils.BaseDao;

import java.util.HashMap;
import java.util.List;

/**
 * @title:CourseDao
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class CourseDaoImpl extends BaseDao<Course> implements CourseDao  {

    /**
     * 验证课程id是否存在
     * @param cid
     * @return
     */
    @Override
    public boolean isExistByCourseId(String cid) {
        Course course= this.selectById(cid);
        return course!=null?true:false;
    }

    /**
     * 添加课程
     * @param course
     * @return
     */
    @Override
    public boolean saveCourse(Course course) {
        return this.insert(course);
    }

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @param name 模糊查询条件
     * @return
     */
    @Override
    public List<Course> listPage(Integer page, Integer limit, HashMap<String,String> wheres){
        return this.queryByPage(page,limit,wheres);
    }

    /**
     * 模糊查询数据
     * @param wheres
     * @return
     */
    @Override
    public List<Course> listCourseByName(HashMap<String, String> wheres) {
//        搜索无需进行分页
        return this.listPage(0,0,wheres);
    }

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    @Override
    public Long countByName(HashMap<String, String> wheres) {
        return this.queryCount(wheres);
    }

    /**
     * 通过key删除
     * @param cid
     * @return
     */
    @Override
    public boolean deleteCourse(Integer cid) {
        return this.deleteById(String.valueOf(cid));
    }

    /**
     * 课程编辑
     * @param course
     * @return
     */
    @Override
    public boolean editCourse(Course course) {
        return this.updateByEntity(course);
    }
}


