package com.hcw.dao.impl;

import com.hcw.dao.AdminDao;
import com.hcw.pojo.Admin;
import com.hcw.utils.BaseDao;

import java.util.HashMap;
import java.util.List;

/**
 * @title:AdminDaoImpl
 * @desc:
 * @author:wang
 * @date:2023/9/9 17:26
 */
public class AdminDaoImpl extends BaseDao<Admin> implements AdminDao {

    /**
     * 管理员登陆,不存在数据时返回null
     *
     * @param admin
     * @return
     */
    @Override
    public Admin adminLogin(Admin admin) {
        HashMap<String, String> map = new HashMap<>();
        map.put("apassword", admin.getApassword());
        map.put("aname", admin.getAname());
        List<Admin> admins = this.queryByColumnNames(map);
        return admins != null && admins.size() > 0 ? admins.get(0) : null;
    }
}
