package com.hcw.dao.impl;

import com.hcw.dao.CollegeDao;
import com.hcw.pojo.College;
import com.hcw.utils.BaseDao;

import java.util.List;

/**
 * @title:TeacherDao
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public class CollegeDaoImpl extends BaseDao<College> implements CollegeDao {

    /**
     * 全部学院数据
     * @return
     */
    @Override
    public List<College> listSelector() {
        return this.listAll();
    }

    @Override
    public College getCollegeById(Integer collegeId) {
        return this.selectById(String.valueOf(collegeId));
    }
}
