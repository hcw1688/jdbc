package com.hcw.dao;

import com.hcw.pojo.Teacher;
import com.hcw.pojo.Teacher;

import java.util.HashMap;
import java.util.List;

/**
 * @title:TeacherDao
 * @desc:
 * @author:wang
 * @date:2023/9/9 11:23
 */
public interface TeacherDao {
    /**
     * 教师登陆校验，不存在数据时返回null
     *
     * @param teacher
     * @return
     */
    public Teacher teacherLogin(Teacher teacher);

    /**
     * 插入数据
     * @param teacher
     * @return
     */
    public boolean teacherSave(Teacher teacher);

    /**
     * 查询教师编号是否存在
     * @param tid
     * @return
     */
    public boolean isExistByTeacherId(String tid);

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @return
     */
    List<Teacher> listPage(Integer page, Integer limit, HashMap<String,String> wheres);

    /**
     * 模糊查询数据
     * @param wheres
     * @return
     */
    List<Teacher> listTeacherByName(HashMap<String,String> wheres);

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    Long countByName(HashMap<String,String> wheres);

    /**
     * 通过key删除
     * @param tid
     * @return
     */
    boolean deleteTeacher(Integer tid);


    /**
     * 教师编辑
     * @param teacher
     * @return
     */
    boolean editTeacher(Teacher teacher);
}
