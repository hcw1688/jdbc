package com.hcw.dao;

import com.hcw.pojo.Admin;

/**
 * @title:AdminDao
 * @desc:
 * @author:wang
 * @date:2023/9/9 11:22
 */
public interface AdminDao {

    /**
     * 管理员登陆,不存在数据时返回null
     *
     * @param admin
     * @return
     */
    public Admin adminLogin(Admin admin);
}
