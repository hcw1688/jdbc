package com.hcw.dao;

import com.hcw.pojo.Course;

import java.util.HashMap;
import java.util.List;

/**
 * @title:CourseDao
 * @desc:
 * @author:wang
 * @date:2023/9/14 16:29
 */
public interface CourseDao {
    /**
     * 验证课程id是否存在
     *
     * @param cid
     * @return
     */
    boolean isExistByCourseId(String cid);

    /**
     * 添加课程
     * @param course
     * @return
     */
    boolean saveCourse(Course course);

    /**
     * 获取分页数据
     * @param page 页数
     * @param limit 每页限制数
     * @param name 模糊查询条件
     * @return
     */
    List<Course> listPage(Integer page, Integer limit, HashMap<String,String> wheres);

    /**
     * 模糊查询数据
     * @param wheres
     * @return
     */
    List<Course> listCourseByName(HashMap<String,String> wheres);

    /**
     * 通过条件统计数据条目方法
     * @param wheres
     * @return
     */
    Long countByName(HashMap<String,String> wheres);

    /**
     * 通过key删除
     * @param cid
     * @return
     */
    boolean deleteCourse(Integer cid);


    /**
     * 课程编辑
     * @param course
     * @return
     */
    boolean editCourse(Course course);

}
