package com.hcw.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @title:RespDto
 * @desc: 数据响应对象
 * @author:wang
 * @date:2023/9/17 14:21
 */
public class RespDTO implements Serializable {
    private String msg;
    private Long count;
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    private List<Object> data;

    public String getMsg() {
        return msg;
    }

    public static RespDTO ok(String msg, List data, Long count) {
        RespDTO pojo = new RespDTO();
        pojo.setMsg(msg);
        pojo.setData(data);
        pojo.setCode(0);
        pojo.setCount(count);
        return pojo;
    }

    public static Object error(String msg) {
        RespDTO pojo = new RespDTO();
        pojo.setMsg(msg);
        pojo.setCode(1);
        return pojo;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }
}
