package com.hcw.pojo;

import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

/**
 * @title:Course
 * @desc:课程实体
 * @author:wang
 * @date:2023/9/14 16:25
 */
@Table("course")
public class Course {
    public Course() {
    }
    @Key
    private String cid;
    private String cname;
    private String cintroduction;
    private String type;
    private String belongcoll;
    private String belongpro;

    public Course(String cid, String cname, String cintroduction, String type,
                  String belongcoll, String belongpro) {
        this.cid = cid;
        this.cname = cname;
        this.cintroduction = cintroduction;
        this.type = type;
        this.belongcoll = belongcoll;
        this.belongpro = belongpro;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCintroduction() {
        return cintroduction;
    }

    public void setCintroduction(String cintroduction) {
        this.cintroduction = cintroduction;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBelongcoll() {
        return belongcoll;
    }

    public void setBelongcoll(String belongcoll) {
        this.belongcoll = belongcoll;
    }

    public String getBelongpro() {
        return belongpro;
    }

    public void setBelongpro(String belongpro) {
        this.belongpro = belongpro;
    }
}
