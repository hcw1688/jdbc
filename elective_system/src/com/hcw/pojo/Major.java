package com.hcw.pojo;

import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

/**
 * @title:Major
 * @desc:
 * @author:wang
 * @date:2023/9/14 15:39
 */
@Table("major")
public class Major {
    @Key
    private Integer id;
    private String mname;
    private Integer collegeid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public Integer getCollegeid() {
        return collegeid;
    }

    public void setCollegeid(Integer collegeid) {
        this.collegeid = collegeid;
    }
}
