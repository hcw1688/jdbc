package com.hcw.pojo;

import com.hcw.utils.Interface.Table;

/**
 * @title:Admin
 * @desc:
 * @author:wang
 * @date:2023/9/9 16:36
 */
@Table("admin")
public class Admin {

    private String aname;

    private String apassword;

    public Admin() {
    }

    public Admin(String aname, String apassword) {
        this.aname = aname;
        this.apassword = apassword;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getApassword() {
        return apassword;
    }

    public void setApassword(String apassword) {
        this.apassword = apassword;
    }


}
