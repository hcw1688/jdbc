package com.hcw.pojo;

import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

/**
 * @title:College
 * @desc:
 * @author:wang
 * @date:2023/9/14 15:38
 */
@Table("college")
public class College {
    @Key
    private Integer id;
    private String title;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

