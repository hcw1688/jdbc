package com.hcw.pojo;

import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

/**
 * @title:Clazz
 * @desc:班级
 * @author:wang
 * @date:2023/9/20 9:16
 */
@Table("clazz")
public class Clazz {
    public Clazz() {
    }

    public Clazz(Integer id, String className, Integer majorId) {
        this.id = id;
        this.className = className;
        this.majorId = majorId;
    }

    @Key
    private Integer id;

    private String className;

    private Integer majorId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getMajorId() {
        return majorId;
    }

    public void setMajorId(Integer majorId) {
        this.majorId = majorId;
    }
}
