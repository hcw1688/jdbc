package com.hcw.pojo;

import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

/**
 * @title:Admin
 * @desc:
 * @author:wang
 * @date:2023/9/9 16:37
 */
@Table("teacher")
public class Teacher {
    @Key
    private String tid;
    private String tname;
    private String tpassword;
    private String tsex;
    private String introduction;

    public Teacher() {
    }

    public Teacher(String tid, String tname, String tpassword, String tsex, String introduction) {
        this.tid = tid;
        this.tname = tname;
        this.tpassword = tpassword;
        this.tsex = tsex;
        this.introduction = introduction;
    }

    public Teacher(String tid, String tpassword) {
        this.tid = tid;
        this.tpassword = tpassword;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTpassword() {
        return tpassword;
    }

    public void setTpassword(String tpassword) {
        this.tpassword = tpassword;
    }

    public String getTsex() {
        return tsex;
    }

    public void setTsex(String tsex) {
        this.tsex = tsex;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "tid='" + tid + '\'' +
                ", tname='" + tname + '\'' +
                ", tpassword='" + tpassword + '\'' +
                ", tsex='" + tsex + '\'' +
                ", introduction='" + introduction + '\'' +
                '}';
    }
}
