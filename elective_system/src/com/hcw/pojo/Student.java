package com.hcw.pojo;

import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

/**
 * @title:Admin
 * @desc:
 * @author:wang
 * @date:2023/9/9 16:36
 */
@Table("student")
public class Student {

    @Key
    private String sid;
    private String sname;
    private String sidcard;
    private String ssex;
    private String spassword;
    private String sage;
    private String classr;
    private String profession;
    private String college;

    public Student() {
    }

    public Student(String sid, String sname, String sidcard, String ssex, String spassword, String sage, String classr, String profession, String college) {
        this.sid = sid;
        this.sname = sname;
        this.sidcard = sidcard;
        this.ssex = ssex;
        this.spassword = spassword;
        this.sage = sage;
        this.classr = classr;
        this.profession = profession;
        this.college = college;
    }

    public Student(String sid, String spassword) {
        this.sid = sid;
        this.spassword = spassword;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getSidcard() {
        return sidcard;
    }

    public void setSidcard(String sidcard) {
        this.sidcard = sidcard;
    }

    public String getSsex() {
        return ssex;
    }

    public void setSsex(String ssex) {
        this.ssex = ssex;
    }

    public String getSpassword() {
        return spassword;
    }

    public void setSpassword(String spassword) {
        this.spassword = spassword;
    }

    public String getSage() {
        return sage;
    }

    public void setSage(String sage) {
        this.sage = sage;
    }

    public String getClassr() {
        return classr;
    }

    public void setClassr(String classr) {
        this.classr = classr;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sid='" + sid + '\'' +
                ", sname='" + sname + '\'' +
                ", sidcard='" + sidcard + '\'' +
                ", ssex='" + ssex + '\'' +
                ", spassword='" + spassword + '\'' +
                ", sage='" + sage + '\'' +
                ", classr='" + classr + '\'' +
                ", profession='" + profession + '\'' +
                ", college='" + college + '\'' +
                '}';
    }
}
