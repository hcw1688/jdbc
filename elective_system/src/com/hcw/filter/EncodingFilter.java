package com.hcw.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @title:EncodingFilter
 * @desc:
 * @author:wang
 * @date:2023/9/20 14:47
 */
@WebFilter("/*")
public class EncodingFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    //过滤的方法
    public void doFilter(ServletRequest servletRequest, ServletResponse
            servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //1.获取【请求】方法
        String method = request.getMethod();
        System.out.println(method);
        //2.处理post请求中文乱码
        if ("POST".equals(method)) {
            request.setCharacterEncoding("utf-8");
        }
        //放行
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
