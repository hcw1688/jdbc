package com.hcw.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @title:AdminLoginFilter
 * @desc:
 * @author:wang
 * @date:2023/9/20 14:49
 */
@WebFilter("/admin/*")
public class AdminLoginFilter implements Filter {

    List<String> urls= Arrays.asList(
            "adminServlet"
            ,"classServlet"
            ,"collegeServlet"
            ,"courseServlet"
            ,"majorServlet"
            ,"studentServlet"
            ,"teacherServlet"
            ,"code"
    );



    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse
            servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

//        String url=request.getRequestURL().toString();
//        if(!urls.contains(url)){
//            request.setAttribute("msg", "无效访问！");
//            return;
//        }

        //校验是否越权访问（通过来源页校验）
//        String ref= request.getHeader("Referer");
//        if(ref==null||ref.equals("")){
//            response.getWriter().write("请通过合法渠道访问页面!");
//            return;
//        }

        //获取session中的数据
        HttpSession session = request.getSession();
        String aname = (String) session.getAttribute("aname");
        System.out.println(aname);
        //判断
        if (aname != null) {
            //登录状态
            filterChain.doFilter(request, response);
        } else {
            //没有登录
            request.setAttribute("msg", "请先登录");
            request.setAttribute("role", "admin");
            //跳转页面
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}

