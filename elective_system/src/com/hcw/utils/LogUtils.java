package com.hcw.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @title:LogUtils
 * @desc:记录日志到文件
 * @author:wang
 * @date:2023/9/13 16:25
 */
public class LogUtils {

    /**
     * 配置为相对路径
     */
    private static final String Path = "F:/Start/QR_PX/elective_system/run.log";

    /**
     * 追加写入日志文件
     *
     * @param msg
     */
    public static void info(String msg) {
        File file = new File(Path);
        //            日期格式化
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        try {
            FileWriter fw = new FileWriter(file, true);
//            记录时间
            fw.write(LocalDateTime.now().format(df) + "------------------------------------------\n");
            //写入异常信息
            fw.write(msg);
            fw.write("\n");
            fw.close();
        } catch (IOException e) {
            System.out.println("文件路径错误！");
        }
    }

    /**
     * 自定义异常信息
     *
     * @param msg
     */
    public static void error(String msg) {
        info(msg);
    }

    /**
     * 发生异常写入溯源信息
     *
     * @param e
     */
    public static void error(Exception e) {
        File file = new File(Path);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter(file, true));
//            记录时间
            printWriter.write(LocalDateTime.now().format(df) + "------------------------------------------\n");
//            写入异常信息
            printWriter.write(e.getMessage());
            e.printStackTrace(printWriter);
//              空行
            printWriter.write("\n");
            printWriter.close();

        } catch (IOException io) {
            System.out.println("文件路径错误！");
        }
    }
}
