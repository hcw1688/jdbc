package com.hcw.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;

/**
 * 反射动态拼接SQL
 * DDL:数据定义 DQL:数据查询 DCL:数据控制 DML:数据操作
 * 现在只有一些基础的crud操作
 *
 * @author wang
 */
public class SqlAssemblyTools {

    /**
     * SQL添加语句
     *
     * @param PojoTClass
     * @param obj
     * @return
     */
//    public static String insertAssembly(Class PojoTClass, Object obj) {
//        //获取实体类上的注解
//        String tabName = AnnotationUtils.getTableName(PojoTClass);
//        String sql = "insert into " + tabName + "(%s) values(%s)";
////		遍历属性字段(包括私有字段)
//        Field[] fields = PojoTClass.getDeclaredFields();
//        String columnNames = "", columnValues = "";
//        for (Field field : fields) {
//            field.setAccessible(true);
//            Object tempValue = null;
//            try {
////			    获取属性值
//                tempValue = field.get(obj);
//            } catch (IllegalAccessException e) {
//                System.out.println("获取" + field.getName() + "属性值时出现异常！");
//            }
//            if (tempValue == null) {
//                continue;
//            }
//            columnValues += "'" + tempValue + "',";
//            columnNames += field.getName() + ",";
//        }
////        去掉字符串最后一个符号","
//        columnValues = columnValues.substring(0, columnValues.length() - 1);
//        columnNames = columnNames.substring(0, columnNames.length() - 1);
////        组装字符串
//        sql = String.format(sql, columnNames, columnValues);
//        System.out.println(sql);
//        return sql;
//    }


    /**
     * SQL添加语句(优化）
     *
     * @param obj
     * @return
     */
    public static String insertAssembly(Object obj) {
        Class projoClass = obj.getClass();
        //获取实体类上的注解
        String tabName = AnnotationUtils.getTableName(projoClass);
        String sql = "insert into " + tabName + "(%s) values(%s)";
//		遍历属性字段(包括私有字段)
        Field[] fields = projoClass.getDeclaredFields();
        String columnNames = "", columnValues = "";
        for (Field field : fields) {
            field.setAccessible(true);
            Object tempValue = null;
            try {
//			    获取属性值
                tempValue = field.get(obj);
            } catch (IllegalAccessException e) {
                System.out.println("获取" + field.getName() + "属性值时出现异常！");
            }
            if (tempValue == null) {
                continue;
            }
            columnValues += "'" + tempValue + "',";
            columnNames += field.getName() + ",";
        }
//        去掉字符串最后一个符号","
        columnValues = columnValues.substring(0, columnValues.length() - 1);
        columnNames = columnNames.substring(0, columnNames.length() - 1);
//        组装字符串
        sql = String.format(sql, columnNames, columnValues);
        System.out.println(sql);
        return sql;
    }

    /**
     * 更新
     * 反射获取实例中的属性名和属性值，拼接成更新语句
     *
     * @return
     */
    public static String updateByEntityAssembly(Object TPojo)
            throws IllegalAccessException, NoSuchFieldException {
        Class tClass = TPojo.getClass();
        String wherePars = "";
        String KeyName = AnnotationUtils.getPrimaryKeyValue(tClass);
        String sql = "update " + AnnotationUtils.getTableName(tClass) + " set %s where " + KeyName + "=%s";
//		取得属性名集合
        Field[] fields = tClass.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
//			取得属性中的value值
            Object columnValue = field.get(TPojo);
//			属性值为空时，不进行更新
            if (columnValue == null) {
                continue;
            }
//            组装name="www",age="123",...
            wherePars += field.getName() + "=" + "'" + columnValue + "',";
        }
//      去掉字符串最后一个符号","
        wherePars = wherePars.substring(0, wherePars.length() - 1);
//      pojo主键字段属性名
        String keyPropertyName = AnnotationUtils.getKeyPropertyName(tClass);
//		拿到key值
        Field field = TPojo.getClass().getDeclaredField(keyPropertyName);
        field.setAccessible(true);
        String key = field.get(TPojo).toString();
//      组装sql
        sql = String.format(sql, wherePars, key);
        System.out.println(sql);
        return sql;
    }

    public static String deleteByIdAssembly(Class PojoTClass, String id) {
//        获取主键字段名
        String KeyName = AnnotationUtils.getPrimaryKeyValue(PojoTClass);
        String sql = "DELETE FROM " + AnnotationUtils.getTableName(PojoTClass) + " WHERE " + KeyName + " = " + id;
        System.out.println(sql);
        return sql;
    }

    /**
     * id查找
     *
     * @param PojoTClass
     * @param key
     * @return
     */
    public static String selectByIdAssembly(Class PojoTClass, String key) {
        if (null == key || key.equals("")) {
            throw new NullPointerException();
        }
        String KeyName = AnnotationUtils.getPrimaryKeyValue(PojoTClass);
        String sql = "SELECT * FROM " + AnnotationUtils.getTableName(PojoTClass) + " WHERE " + KeyName + "=" + key;
        System.out.println(sql);
        return sql;
    }

    public static String selectListAssembly(Class PojoTClass, String ColumnNames) {
        String sql = "SELECT " + ColumnNames + " FROM " + AnnotationUtils.getTableName(PojoTClass);
        System.out.println(sql);
        return sql;
    }

    /**
     * 全部数据
     *
     * @param PojoTClass
     * @return
     */
    public static String selectListAssembly(Class PojoTClass) {
        String sql = "SELECT * FROM " + AnnotationUtils.getTableName(PojoTClass);
        System.out.println(sql);
        return sql;
    }


    public static String listByWhereParameters(Class PojoTClass, HashMap<String, String> wheres) {
//        不使用模糊查询条件
        String whereString = getWhereStringFromMap(wheres, false);
        String sql = "SELECT * FROM " + AnnotationUtils.getTableName(PojoTClass) + whereString;
        System.out.println(sql);
        return sql;
    }

    /**
     * 分页查询数据拼接
     *
     * @param PojoTClass
     * @param page
     * @param limit
     * @param wheres
     * @return
     */
    public static String queryByPageAssembly(Class PojoTClass, Integer page, Integer limit, HashMap<String, String> wheres) {
        String sql = "SELECT * FROM " + AnnotationUtils.getTableName(PojoTClass);
//        拼接where语句,模糊查询
        sql += getWhereStringFromMap(wheres, true);

        if (limit > 0) {
            int skip = (page - 1) * limit;
            sql += " limit " + skip + " , " + limit;
        }
        System.out.println(sql);
        return sql;
    }


    /**
     * 根据条件获取数据统计
     *
     * @param PojoTClass
     * @param wheres
     * @return
     */
    public static String countQueryAssembly(Class PojoTClass, HashMap<String, String> wheres) {
        String sql = "SELECT COUNT(" + AnnotationUtils.getPrimaryKeyValue(PojoTClass) + ") FROM " + AnnotationUtils.getTableName(PojoTClass);
//      拼接where语句
        sql += getWhereStringFromMap(wheres, true);
        System.out.println(sql);
        return sql;
    }

    /**
     * 从 Map 中获取 SQL WHERE 条件字符串(包含WHERE关键字)
     *
     * @param wheres
     * @param isLike 是否为模糊查询
     * @return
     */
    private static String getWhereStringFromMap(HashMap<String, String> wheres, boolean isLike) {
        Iterator<String> whereKey = wheres.keySet().iterator();
        StringBuilder sb = new StringBuilder();
        if (whereKey.hasNext() == false) {
            return "";
        }
        while (whereKey.hasNext()) {
            String key = whereKey.next();
            String value = wheres.get(key);

            if (null == value || "".equals(value)) {
                continue;
            }

            if (!sb.toString().contains("WHERE")) {
                sb.append(" WHERE ");
            }
//          判断是否为模糊查询
            if (isLike && value != null) {
                sb.append(key + " like " + "'%" + value + "%'");
            } else if (value != null) {
                sb.append(key + " = " + "'" + value + "'");
            }
//            存在下一个参数
            if (whereKey.hasNext()) {
                sb.append(" AND ");
            }
        }
        return sb.toString();
    }
}


