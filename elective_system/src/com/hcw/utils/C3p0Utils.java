package com.hcw.utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;

/**
 * 基于c3p0数据源配置连接池
 * 官网地址：https://www.mchange.com/projects/c3p0
 * @author wang
 */
public class C3p0Utils {

    /**
     * 第一种方式
    public static DataSource ds = null;
    private static final String driver = "com.mysql.cj.jdbc.Driver";
    private static final String url = "jdbc:mysql://localhost:3306/jdbc?serverTimezone=UTC";
    private static final String user = "root";
    private static final String password = "123456";
    private static final int Init_POOL_SIZE = 5;
    private static final int MAX_POOL_SIZE = 15;

    static {
        ComboPooledDataSource cpds = new ComboPooledDataSource();

        try {
            cpds.setDriverClass("com.mysql.cj.jdbc.Driver");
            cpds.setJdbcUrl("jdbc:mysql://localhost:3306/jdbc?serverTimezone=UTC");
            cpds.setUser("root");
            cpds.setPassword("123456");
            cpds.setInitialPoolSize(5);
            cpds.setMaxPoolSize(15);
            ds = cpds;
        } catch (Exception var2) {
            throw new ExceptionInInitializerError(var2);
        }
    }
     */

    /**
     * 第二种方式 (单例模式)
     */
    private static final DataSource cpds = new ComboPooledDataSource();

    public static DataSource getDataSource() {
        return cpds;
    }

}