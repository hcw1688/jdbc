package com.hcw.utils;

import java.util.HashMap;
import java.util.List;

/**
 * @title:BaseDao
 * @desc:公共的dao接口，使用工具类从这里开始
 * @author:wang
 * @date:2023/9/8 11:13
 */
public class BaseDao<T> extends DatabaseUtils<T>{
    public BaseDao() {
        super();
    }

    @Override
    public List<T> listAll(String columnNames)  {
        return super.listAll(columnNames);
    }

    @Override
    public boolean insert(T entity) {
        return super.insert(entity);
    }

    @Override
    public boolean deleteById(String id)  {
        return super.deleteById(id);
    }

    @Override
    public boolean updateByEntity(T entity) {
        return super.updateByEntity(entity);
    }

    @Override
    public T selectById(String id)  {
        return super.selectById(id);
    }

    @Override
    public List<T> queryByColumnNames(HashMap<String, String> wheres)  {
        return super.queryByColumnNames(wheres);
    }

    @Override
    public List<T> listAll()  {
        return super.listAll();
    }
}
