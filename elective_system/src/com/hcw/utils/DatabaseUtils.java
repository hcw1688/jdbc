package com.hcw.utils;

import com.hcw.utils.Interface.ICrud;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 这里仅实现简单对象的crud操作
 *
 * @param <T>
 * @author wang
 */
public class DatabaseUtils<T> implements ICrud<T> {
    private QueryRunner query;
    private Class<T> PojoClass;

    public DatabaseUtils() {
        this.PojoClass = GetTClass();
        System.out.println("初始化类型:" + PojoClass.getSimpleName());
    }

    /**
     * 获取T类型
     *
     * @return
     */
    private Class<T> GetTClass() {
        // 获取当前类的直接父类的类型，包括泛型信息
        ParameterizedType parameterizedType = (ParameterizedType)
                getClass().getGenericSuperclass();
        // 获取泛型参数的类型数组，并将第一个元素转换为Class对象
        Class entityClass = (Class<T>) parameterizedType.getActualTypeArguments()[0];
        // 返回泛型参数的Class对象
        return entityClass;
    }

    @Override
    public boolean insert(T entity) {
        query = new QueryRunner(C3p0Utils.getDataSource());
        int affectedRows = 0;
        String sql = SqlAssemblyTools.insertAssembly(entity);
        try {
            affectedRows = query.update(sql);
        } catch (Exception e) {
            LogUtils.error(e);
        }
        return affectedRows > 0 ? true : false;
    }

    @Override
    public boolean deleteById(String id) {
        QueryRunner runner = new QueryRunner(C3p0Utils.getDataSource());
        String sql = SqlAssemblyTools.deleteByIdAssembly(PojoClass, id);
        int affectedRows = 0;
        try {
            affectedRows = runner.update(sql);
        } catch (SQLException e) {
            LogUtils.error(e);
        }
        return affectedRows > 0 ? true : false;
    }

    /**
     * 根据对象实例更新数据,请确保key字段不能为空(优化)
     *
     * @param entity
     * @return
     */
    @Override
    public boolean updateByEntity(T entity) {
        QueryRunner runner = new QueryRunner(C3p0Utils.getDataSource());
        int affectedRows = 0;
        try {
            String sql = SqlAssemblyTools.updateByEntityAssembly(entity);
            affectedRows = runner.update(sql);
        } catch (SQLException e) {
            LogUtils.error(e);
        } catch (NoSuchFieldException e) {
            LogUtils.error(e);
        } catch (IllegalAccessException e) {
            LogUtils.error(e);
        }
        return affectedRows > 0 ? true : false;
    }

    /**
     * 根据key查找
     *
     * @param id
     * @return
     */
    @Override
    public T selectById(String id) {
        query = new QueryRunner(C3p0Utils.getDataSource());
        String sql = SqlAssemblyTools.selectByIdAssembly(PojoClass, String.valueOf(id));
        T pojo = null;
        try {
            pojo = (T) query.query(sql, new BeanHandler(PojoClass));
        } catch (SQLException e) {
            LogUtils.error(e);
        }
        return pojo;
    }

    /**
     * 返回参数条件数据，没有数据时返回null
     *
     * @param wheres
     * @return
     */
    @Override
    public List<T> queryByColumnNames(HashMap<String, String> wheres) {
        query = new QueryRunner(C3p0Utils.getDataSource());
        String sql = SqlAssemblyTools.listByWhereParameters(PojoClass, wheres);
        List<T> list = null;
        try {
            list = (List<T>) query.query(sql, new BeanListHandler(PojoClass));
        } catch (SQLException e) {
            LogUtils.error(e);
        }
        return list;
    }

    /**
     * 分页获取数据
     *
     * @param page
     * @param limit
     * @param mapWhere
     * @return
     */
    @Override
    public List<T> queryByPage(Integer page, Integer limit, HashMap<String, String> mapWhere) {
        query = new QueryRunner(C3p0Utils.getDataSource());
        String sql = SqlAssemblyTools.queryByPageAssembly(PojoClass, page, limit, mapWhere);
        List<T> list = new ArrayList<>();
        try {
//            int a=1/0;
            list = (List<T>) query.query(sql, new BeanListHandler(PojoClass));
        } catch (SQLException e) {
            LogUtils.error(e);
        } catch (ArithmeticException e) {
            LogUtils.error(e);
        }
        return list;
    }

    /**
     * 根据条件获取数据统计
     *
     * @param wheres
     * @return
     */
    @Override
    public Long queryCount(HashMap<String, String> wheres) {
        query = new QueryRunner(C3p0Utils.getDataSource());
        String sql = SqlAssemblyTools.countQueryAssembly(PojoClass, wheres);
        Long count = null;
        try {
            count = query.query(sql, new ScalarHandler<>());
        } catch (SQLException e) {
            LogUtils.error(e);
        }
        return count;
    }

    /**
     * 获取全部数据
     *
     * @return
     */
    @Override
    public List<T> listAll(String ColumnNames) {
        query = new QueryRunner(C3p0Utils.getDataSource());
        String sql = SqlAssemblyTools.selectListAssembly(PojoClass, ColumnNames);
        List<T> list = new ArrayList<>();
        try {
            list = (List<T>) query.query(sql, new BeanListHandler(PojoClass));
        } catch (SQLException e) {
            LogUtils.error(e);
        }
        return list;
    }

    /**
     * 获取全部数据
     *
     * @return
     */
    @Override
    public List<T> listAll() {
        query = new QueryRunner(C3p0Utils.getDataSource());
        String sql = SqlAssemblyTools.selectListAssembly(PojoClass);
        List<T> list = null;
        try {
            list = (List<T>) query.query(sql, new BeanListHandler(PojoClass));
        } catch (SQLException e) {
            LogUtils.error(e);
        }
        return list;
    }

}
