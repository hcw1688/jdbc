package com.hcw.utils;

import com.hcw.utils.Interface.Key;
import com.hcw.utils.Interface.Table;

import java.lang.reflect.Field;

/**
 * 注解工具类
 *
 * @author wang
 */
public class AnnotationUtils {
    /**
     * 获取类名上的注解值
     *
     * @param PojoClass
     * @return
     */
    private static String getClassAnnotationValue(Class PojoClass) {
        Table table = (Table) PojoClass.getAnnotation(Table.class);
        return table.value().trim();
    }

    /**
     * 获取数据表名
     *
     * @param PojoClass
     * @return table name
     */
    public static String getTableName(Class PojoClass) {
        String value = getClassAnnotationValue(PojoClass);
//       未定义注解时，默认以类名为表名
        if (null == value || value.equals("")) {
            value = PojoClass.getSimpleName();
        }
        return "`" + value + "`";
    }

    /**
     * 扫描标记为主键注解的属性字段，返回Field对象
     *
     * @param PojoClass
     * @return
     */
    private static Field queryKeyFieldByEntityClass(Class PojoClass) {
        Field[] fields = PojoClass.getDeclaredFields();
//        遍历对象属性集合
        for (Field f : fields) {
            f.setAccessible(true);
//            验证字段是否存在注解
            if (f.isAnnotationPresent(Key.class)) {
                return f;
            }
        }
        return null;
    }


    /**
     * 获取主键字段注解值(数据库主键字段列名)
     *
     * @param PojoClass
     * @return
     */
    public static String getPrimaryKeyValue(Class PojoClass) {
//        查找key字段
        Field keyField = queryKeyFieldByEntityClass(PojoClass);
        Key annotation = keyField.getAnnotation(Key.class);

        //       取得注解属性(ColumnName)值
        String MappingColumnName = annotation.ColumnName();
        if (null == MappingColumnName || MappingColumnName.equals("")) {
            MappingColumnName = annotation.value();
        }

//        默认值,属性名，当作数据库key的字段名
        if (null == MappingColumnName || MappingColumnName.equals("")) {
            MappingColumnName = keyField.getName();
        }
        return MappingColumnName;
    }


    /**
     * 获得pojo类中主键字段的属性名
     *
     * @param PojoClass
     * @return
     */
    public static String getKeyPropertyName(Class PojoClass) {
        Field field = queryKeyFieldByEntityClass(PojoClass);
        return field.getName();
    }

}
