package com.hcw.utils.Interface;

import java.util.HashMap;
import java.util.List;

/**
 * CRUD接口
 * 
 * @author wang
 *
 */
public interface ICrud<T> {
	/**
	 * 插入数据
	 * @param entity
	 * @return
	 * @
	 */
	boolean insert(T entity);

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 * @
	 */
	boolean deleteById(String id);

	/**
	 *更新
	 * @param entity
	 * @return
	 */
	boolean updateByEntity(T entity);

	/**
	 * 根据条件列名获取全部
	 * @return
	 */
	List<T> listAll(String ColumnNames);

	/**
	 * 全部数据
	 * @return
	 */
	List<T> listAll();

	/**
	 * 通过id获取数据
	 * @param id
	 * @return
	 */
	T selectById(String id);

	/**
	 * 通过多条件获取
	 * @param wheres
	 * @return
	 */
	List<T> queryByColumnNames(HashMap<String,String> wheres);

	/**
	 * 分页获取数据
	 * @param page
	 * @param limit
	 * @param name
	 * @return
	 */
	List<T> queryByPage(Integer page, Integer limit, HashMap<String,String> mapWhere);

	/**
	 * 根据条件获取数据统计
	 * @param wheres
	 * @return
	 */
	Long queryCount(HashMap<String,String> mapWhere);
}
