package com.hcw.utils.Interface;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * �����ֶ�ע��
 * ����ͨ��ע�����ã��������ݿ⡣
 * @author wang
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Field{
// ��ȡ�������ֶ�ӳ��
 String ColumnName() default "";
// ��������
 String type() default  "";
// ���ݳ���
 int length() default  0;
}
