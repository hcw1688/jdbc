<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo</title>
    <link href="/utils/layui/css/layui.css" rel="stylesheet">
</head>
<body>
<form class="layui-form" style="width:60%;" action="" lay-filter="demo-val-filter">
    <div class="layui-form-item" id="sid_hidde_model">
        <label class="layui-form-label">学号</label>
        <div class="layui-input-inline layui-input-wrap" style="width: 200px;">
            <input type="number" id="Sid" name="sid" maxlength="4" required placeholder="请输入学生编号"
                   autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-text-em" id="checkCId">长度：12位数字</div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">身份证号</label>
        <div class="layui-input-block">
            <input type="text" name="sidcard" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">学生名称</label>
        <div class="layui-input-block">
            <input type="text" name="sname" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-block">
            <select name="ssex">
                <option value="请选择性别"></option>
                <option value="男">男</option>
                <option value="女">女</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">年龄</label>
        <div class="layui-input-block">
            <input type="number" name="sage" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">学院</label>
        <div class="layui-input-block">
            <select name="belongcoll" id="belongcoll" lay-filter="aihao">
                <option value="请选择归属学院">请选择归属学院</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">专业</label>
        <div class="layui-input-block">
            <select name="belongpro" id="belongpro" lay-filter="mojorEvent">
                <option value="请选择专业">请选择专业</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">班级</label>
        <div class="layui-input-block">
            <select name="classr" id="classr">
                <option value="请选择班级">请选择班级</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-block">
            <input type="password" name="spassword"  autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn" lay-submit lay-filter="demo-val">立即提交</button>
            <button type="reset" style="background-color:#66a2fe;" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/utils/layui/layui.js"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.$;
        var form = layui.form;

        // 编辑数据通过域对象传入
        var modelData = localStorage.getItem('edit_data');
        var editJsonData = JSON.parse(modelData);

        // 获取学院选择器数据
        $.get("/collegeServlet?flag=getColleges", function (result) {
            $.each(result, function (index, item) {
                const option = new Option(item.title, item.id);
                //存在编辑数据，选中数据
                if (editJsonData.college === item.title) {
                    option.selected = true;
                }
                $('#belongcoll').append(option);
            });
            layui.form.render("select");
        }, "json");

        // 触发专业选择器
        form.on('select(aihao)', function () {
            // 获取专业数据并绑定到选择器
            getMajorSelectorData();
        });

        // 获取专业选择器获取
        function getMajorSelectorData() {
            var collegeId = $("#belongcoll").val();
            console.log("进入二级菜单")
            $("#belongpro").empty();
            $.get("/majorServlet?flag=getMajors&collegeId=" + collegeId, function (result) {
                $.each(result, function (index, item) {
                    const option = new Option(item.mname, item.id);
                    //存在编辑数据，就选中
                    if (editJsonData.profession === item.mname) {
                        option.selected = true;
                    }
                    $('#belongpro').append(option);
                });
                layui.form.render("select");
            }, "json");
        }

        // 触发班级选择器事件
        form.on('select(mojorEvent)', function () {
            // 获取班级数据并绑定
            getClassSelectorData();
        });

        // 获取班级选择器数据
        function getClassSelectorData() {
            var majorId = $("#belongpro").val();
            console.log("进入三级菜单")
            $("#classr").empty();
            $.get("/classServlet?flag=getClasss&majorId=" + majorId, function (result) {
                $.each(result, function (index, item) {
                    const option = new Option(item.className, item.id);
                    //存在编辑数据，就选中
                    if (editJsonData.classr === item.className) {
                        option.selected = true;
                    }
                    $('#classr').append(option);
                });
                layui.form.render("select");
            }, "json");
        }


        // 学生编号校验
        var cinput = $("#Sid");
        cinput.blur(function () {
            cinput = $("#Sid");
            console.log("离开！")
            // 清理提示
            $('#checkCId').text("")
            console.log(cinput.val())
            // 学生编号校验
            $.get("/studentServlet?flag=chexckSid&sid=" + cinput.val(), function (result) {
                CheckInformation(result);
            });
        });

        // 自定义校验提示
        function CheckInformation(result) {
            if (result === "true") {
                $('#checkCId').text("学生编号已存在,请重新输入！")
                $("#Sid").focus();//选中输入框
                $('#checkCId').attr("style", "color:red!important;")
            } else if (cinput.val().length < 12 || cinput.val().length > 12) {
                $('#checkCId').text("要求:长度为12位数字！")
                $("#Sid").focus();//选中输入框
                $('#checkCId').attr("style", "color:red!important;")
            } else {
                $('#checkCId').text("学生编号可用")
                $('#checkCId').attr("style", "color:#68c923!important;")
            }
        }


        // 针对添加和编辑，仅以下代码进行区分
        //如果域对象中不存在回显数据，值默为添加操作
        var flag = "add";
        if (modelData === "null" || modelData === "") {
            flag = "add";
        } else {
            // 是编辑操作，隐藏学生id字段
            $('#sid_hidde_model').attr("style", "display:none;")
            flag = "edit";
            // 回显选择器数据
            getMajorSelectorData();
            getClassSelectorData();
            // 回显数据
            form.val("demo-val-filter", JSON.parse(modelData));
        }

        // 提交事件
        form.on('submit(demo-val)', function (data) {
            var field = data.field; // 获取表单字段值
            field.flag = flag;
            $.post("/studentServlet", field, function (result) {
                console.log(result)
                if (result) {
                    layer.msg("操作成功");
                    setTimeout(function () {
                        var index = parent.layer.getFrameIndex(window.name); // 先得到当前 iframe 层的索引
                        parent.layer.close(index); // 再执行关闭
                    }, 600)
                } else {
                    layer.msg("操作失败")
                }
            });
            return false; // 阻止默认 form 跳转
        });
    })
</script>

</body>
</html>