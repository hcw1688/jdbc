<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo</title>
    <link href="/utils/layui/css/layui.css" rel="stylesheet">
</head>
<body>
<form class="layui-form" style="width:60%;" action="" lay-filter="demo-val-filter">

    <div class="layui-form-item" id="tid_hidde_model">
        <label class="layui-form-label">教师编号</label>
        <div class="layui-input-inline layui-input-wrap" style="width: 200px;">
            <input type="number" id="Tid" name="tid" maxlength="4" required placeholder="请输入教师编号"
                   autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-text-em" id="checkCId">长度：4位数字</div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">教师名称</label>
        <div class="layui-input-block">
            <input type="text" name="tname" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-block">
            <input type="password" name="tpassword" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-block">
            <select name="tsex">
                <option value="请选择性别"></option>
                <option value="男">男</option>
                <option value="女">女</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">介绍信息</label>
        <div class="layui-input-block">
            <textarea placeholder="请输入内容" name="introduction" class="layui-textarea"></textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn" lay-submit lay-filter="demo-val">立即提交</button>
            <button type="reset" style="background-color:#66a2fe;" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/utils/layui/layui.js"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.$;
        var form = layui.form;

        // 编辑数据通过域对象传入
        var modelData = localStorage.getItem('edit_data');
        var editJsonData = JSON.parse(modelData);
        console.log(modelData)
        // 教师编号校验
        var cinput = $("#Tid");
        cinput.blur(function () {
            cinput = $("#Tid");
            console.log("离开！")
            // 清理提示
            $('#checkCId').text("")
            console.log(cinput.val())
            // 教师编号校验
            $.get("/teacherServlet?flag=checkTid&tid=" + cinput.val(), function (result) {
                CheckInformation(result);
            });
        });

        // 自定义校验提示
        function CheckInformation(result) {
            if (result === "true") {
                $('#checkCId').text("教师编号已存在,请重新输入！")
                $("#Tid").focus();//选中输入框
                $('#checkCId').attr("style", "color:red!important;")
            } else if (cinput.val().length < 4 || cinput.val().length > 4) {
                $('#checkCId').text("要求:长度为4位数字！")
                $("#Tid").focus();//选中输入框
                $('#checkCId').attr("style", "color:red!important;")
            } else {
                $('#checkCId').text("教师编号可用")
                $('#checkCId').attr("style", "color:#68c923!important;")
            }
        }

        // 针对添加和编辑，仅以下代码进行区分
        //如果域对象中不存在回显数据，值默为添加操作
        var flag = "add";
        if (modelData === "null" || modelData === "") {
            flag = "add";
            console.log("进入添加")
        } else {
            console.log("进入编辑")
            // 是编辑操作，隐藏教师id字段
            $('#tid_hidde_model').attr("style", "display:none;")
            flag = "edit";
            // 回显数据
            form.val("demo-val-filter", JSON.parse(modelData));
        }

        // 提交事件
        form.on('submit(demo-val)', function (data) {
            var field = data.field; // 获取表单字段值
            field.flag = flag;
            $.post("/teacherServlet", field, function (result) {
                console.log(result)
                if (result) {
                    layer.msg("操作成功");
                    setTimeout(function () {
                        var index = parent.layer.getFrameIndex(window.name); // 先得到当前 iframe 层的索引
                        parent.layer.close(index); // 再执行关闭
                    }, 600)
                } else {
                    layer.msg("操作失败")
                }
            });
            return false; // 阻止默认 form 跳转
        });
    })
</script>

</body>
</html>