<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="/utils/scripts/flat-ui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="/StudentInfo/utils/image/favicon.ico" type="image/x-icon"/>
    <meta charset="UTF-8">
    <title>学生信息管理系统</title>
    <script type="text/javascript" src="/utils/js/jquery-3.3.1.min.js"></script>
    <!-- Loading Bootstrap -->
    <link href="/utils/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!-- Loading Flat UI Pro -->
    <link href="/utils/css/flat-ui.css" rel="stylesheet">
    <!-- Loading Flat UI JS -->
    <script type="text/javascript" src="/utils/scripts/flat-ui.min.js"></script>
    <script type='text/javascript' src='/utils/scripts/particles.js'></script>
    <link href="/utils/css/animate.css" rel="stylesheet">
    <link href="/utils/layui/css/layui.css" rel="stylesheet">
</head>

<style>
    .layui-table-body {
        overflow: hidden;
        height: auto !important;
    }

</style>
<body>
<div id="particles-js">
    <canvas class="particles-js-canvas-el" width="1322" height="774" style="width: 100%; height: 100%;"></canvas>
</div>
<jsp:include page="adminLeft.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5>课程管理</h5>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <div style="padding: 16px;">
                            <table class="layui-hide" id="tableElement" lay-filter="test"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("select").select2({
        dropdownCssClass: 'dropdown-inverse'
    });
</script>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <%--        添加+搜索--%>
        <div class="layui-form-item">
            <button class="layui-btn layui-btn-sm" lay-event="add">添加数据</button>
            <div class="layui-inline" style="left:100px;">
                <div class="layui-input-inline layui-input-wrap">
                    <input type="text" name="cname" id="search_value" autocomplete="off"
                           lay-reqtext="请输入课程名称" lay-affix="clear" class="layui-input demo-phone">
                </div>
                <div class="layui-form-mid" style="padding: 0!important;">
                    <button type="button" lay-event="search" style="color:#8554af!important;"
                            class="layui-btn layui-btn-primary"
                            lay-on="get-vercode">搜索
                    </button>
                </div>
            </div>
        </div>
</script>
<script type="text/html" id="barDemo">
    <div class="layui-clear-space">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="remove">删除</a>
    </div>
</script>
<script type="text/javascript" src="/utils/scripts/flat-ui.js"></script>
<script src="/utils/scripts/bganimation.js"></script>

<script src="/utils/layui/layui.js"></script>
<script>
    layui.use(['table'], function () {
        var table = layui.table;
        // 创建渲染实例
        table.render({
            elem: '#tableElement',
            url: '/courseServlet?flag=pageList', // 此处为静态模拟数据，实际使用时需换成真实接口
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            height: 'full-35', // 最大高度减去其他容器已占有的高度差
            css: [ // 重设当前表格样式
                '.layui-table-tool-temp{padding-right: 145px;}'
            ].join(''),
            cellMinWidth: 80,
            totalRow: true, // 开启合计行
            page: true,
            cols: [[
                {type: 'checkbox', fixed: 'left'},
                {field: 'cid', fixed: 'left', width: 100, title: '课程编号', sort: false},
                {field: 'cname', width: 150, title: '课程名称'},
                {field: 'type', width: 100, title: '类型', sort: false},
                {field: 'belongcoll', width: 150, title: '归属学院', sort: false},
                {field: 'belongpro', width: 150, title: '归属专业', sort: false},
                {field: 'cintroduction', width: 150, title: '专业介绍', sort: false},
                {fixed: 'right', title: '操作', width: 134, minWidth: 125, toolbar: '#barDemo'}
            ]],
            error: function (res, msg) {
                console.log(res, msg)
            }
        });

        // 工具栏事件
        table.on('toolbar(test)', function (obj) {
            var id = obj.config.id;
            var checkStatus = table.checkStatus(id);
            var othis = lay(this);
            switch (obj.event) {
                case 'add':
                    console.log(222)
                    var data = checkStatus.data;
                    // layer.alert(layui.util.escape(JSON.stringify(data)));
                    var index = layer.open({
                        type: 2, // page 层类型
                        area: ['800px', '600px'],
                        title: '添加课程',
                        shade: 0.6, // 遮罩透明度
                        shadeClose: true, // 点击遮罩区域，关闭弹层
                        maxmin: true, // 允许全屏最小化
                        anim: 0, // 0-6 的动画形式，-1 不开启
                        content: 'addOrEdit.jsp',
                        success: function (layero, dIndex) {
                            console.log("处理完成")
                            localStorage.setItem('edit_data',null)
                        }, end: function () {
                            table.reload('tableElement');
                        }
                    });
                    break;
                case 'search':
                    var cname = $("#search_value").val();
                    console.log(cname)
                    //执行搜索重载表格
                    table.reload('tableElement', {
                        url: "/courseServlet"
                        // page: {
                        //     curr: 1
                        // }
                        , where: {
                            cname: cname,
                            flag: "search"
                        }
                    });
                    break;
                case 'LAYTABLE_TIPS':
                    layer.alert('自定义工具栏图标按钮');
                    break;
            }
        });

        // 触发单元格工具事件
        table.on('tool(test)', function (obj) { // 双击 toolDouble
            var data = obj.data; // 获得当前行数据
            console.log(data)
            if (obj.event === 'edit') {
                var index = layer.open({
                    type: 2, // page 层类型
                    area: ['800px', '600px'],
                    title: '编辑课程',
                    shade: 0.6, // 遮罩透明度
                    shadeClose: true, // 点击遮罩区域，关闭弹层
                    maxmin: true, // 允许全屏最小化
                    anim: 0, // 0-6 的动画形式，-1 不开启
                    content: 'addOrEdit.jsp',
                    success: function (layero, dIndex) {
                        console.log("处理完成")
                        // 通过全局对象将数据传入编辑页面中
                        localStorage.setItem('edit_data', JSON.stringify(data));
                    }, end: function () {
                        table.reload('tableElement');
                    }
                });
            } else if (obj.event === 'remove') {    //删除操作
                layer.confirm('真的删除行 [id: ' + data.cid + '] 么', function (index) {
                    obj.del(); // 删除对应行（tr）的DOM结构
                    layer.close(index);
                    $.post("/courseServlet?flag=delete&cid=" + data.cid, function (res) {
                        if (res) {
                            layer.msg('删除成功', {icon: 1});
                            table.reload('tableElement'); //重载表格
                        } else {
                            layer.msg('操作失败！', {icon: 0});
                        }
                    })
                });
            }
        });

        // 触发表格复选框选择
        table.on('checkbox(test)', function (obj) {
            console.log(obj)
        });

        // 触发表格单选框选择
        table.on('radio(test)', function (obj) {
            console.log(obj)
        });

        // 行单击事件
        table.on('row(test)', function (obj) {
            //console.log(obj);
            //layer.closeAll('tips');
        });
        // 行双击事件
        table.on('rowDouble(test)', function (obj) {
            console.log(obj);
        });

        // 单元格编辑事件
        table.on('edit(test)', function (obj) {
            var field = obj.field; // 得到字段
            var value = obj.value; // 得到修改后的值
            var data = obj.data; // 得到所在行所有键值
            // 值的校验
            if (field === 'email') {
                if (!/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(obj.value)) {
                    layer.tips('输入的邮箱格式不正确，请重新编辑', this, {tips: 1});
                    return obj.reedit(); // 重新编辑 -- v2.8.0 新增
                }
            }
            // 编辑后续操作，如提交更新请求，以完成真实的数据更新
            // …
            layer.msg('编辑成功', {icon: 1});

            // 其他更新操作
            var update = {};
            update[field] = value;
            obj.update(update);
        });
    });
</script>
</body>
</html>
