<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="/utils/scripts/flat-ui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="/StudentInfo/utils/image/favicon.ico" type="image/x-icon"/>
    <meta charset="UTF-8">
    <title>学生信息管理系统</title>
    <script type="text/javascript" src="/utils/js/jquery-3.3.1.min.js"></script>
    <!-- Loading Bootstrap -->
    <link href="/utils/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!-- Loading Flat UI Pro -->
    <link href="/utils/css/flat-ui.css" rel="stylesheet">
    <!-- Loading Flat UI JS -->
    <script type="text/javascript" src="/utils/scripts/flat-ui.min.js"></script>

    <script type='text/javascript' src='/utils/scripts/particles.js'></script>
    <link href="/utils/css/animate.css" rel="stylesheet">
</head>
<body>
<div id="particles-js">
    <canvas class="particles-js-canvas-el" width="1322" height="774" style="width: 100%; height: 100%;"></canvas>
</div>
<jsp:include page="adminLeft.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h5>添加课程</h5>
            <form action="" method="post" name="form1" id="myForm" onsubmit="return false">
                <h6>课程编号</h6>
                <input type="text" name="cid" id="cid" class="form-control" oninput="value=value.replace(/[^\d]/g,'')"
                       maxlength="5"></input>

                <span id="countt" style="color: #ff0000;"></span>


                <h6>课程名称</h6>
                <input type="text" name="cname" class="form-control"
                       onkeyup="this.value=this.value.replace(/(^\s+)|(\s+$)/g,'');" maxlength="10"></input>
                <h6>课程简介</h6>
                <textarea name="cintroduction" cols="20" rows="6"
                          class="form-control"></textarea>

                <h6>课程类型</h6>
                <select name="type"
                        class="form-control select select-primary select-block mbl">
                    <option value="必修">必修</option>
                    <option value="选修">选修</option>
                </select>

                <h6>系名称</h6>
                <select name="belongcoll"
                        class="form-control select select-primary select-block mbl" id="belongcoll"
                        onchange="changeSelect(this.selectedIndex)">

                </select>

                <h6>专业名称</h6>
                <select name="belongpro"
                        class="form-control select select-primary select-block mbl" id="belongpro">

                </select> <br>

                <input type="submit" value="添加" disabled="disabled" id="sub" class="btn btn-primary btn-wide login-btn" />

            </form>

        </div>
    </div>
</div>


<script>
    $("select").select2({
        dropdownCssClass: 'dropdown-inverse'
    });
</script>

<script>
    $(function () {
        $.get("/collegeServlet?flag=getColleges", function (result) {
            for (var i = 0; i < result.length; i++) {
                var op = $("<option></option>");
                op.val(result[i].id);
                op.text(result[i].title);
                $("#belongcoll").append(op);
            }
        }, "json");

        //加载二级下拉框
        $("#belongcoll").change(function () {
            var collegeId = $("#belongcoll").val();
            $("#belongpro").empty();
            $.get("/majorServlet?flag=getMajors&collegeId=" + collegeId, function (result) {
                for (var i = 0; i < result.length; i++) {
                    var option = $("<option></option>");
                    option.val(result[i].id);
                    option.text(result[i].mname);
                    $("#belongpro").append(option);
                }
            }, "json");
        });

        //Ajax校验课程编号是否存在
        $(function () {
            var cinput = $("#cid");
            var sub = $("#sub");
            cinput.blur(function () {
                var cVal = cinput.val();
                if (cVal != "") { //课程编号不为空串
                    $.get("/courseServlet?flag=chexckCid&cid=" + cVal, function (result) {
                        if (result == "true") { //教师编号已经存在
                            $("#countt").text("课程编号已经存在")
                            $("#countt").css("color", "red");
                            sub.prop("disabled", true);
                        } else {
                            $("#countt").text("课程编号可用")
                            $("#countt").css("color", "green");
                            sub.prop("disabled", false);
                        }
                    });
                } else { //课程编号为空串
                    $("#countt").text("课程编号为空")
                    $("#countt").css("color", "red");
                    sub.prop("disabled", true);
                }
            });
        });

        //Ajax提交表单数据
        $(function () {
            var sub = $("#sub");
            sub.click(function () {
                var param = $("#myForm").serialize();
                alert(param)
                $.post("/courseServlet", "flag=add&" + param, function (result) {
                    if (result == "true") {
                        alert("新增课程成功");
                        location.reload()
                    } else {
                        alert("新增课程失败")
                    }
                });
            });
        });
    });


</script>
<script type="text/javascript" src="/utils/scripts/flat-ui.js"></script>
<script src="/utils/scripts/bganimation.js"></script>
</body>
</html>