<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo</title>
    <link href="/utils/layui/css/layui.css" rel="stylesheet">
</head>
<body>
<form class="layui-form" style="width:60%;" action="" lay-filter="demo-val-filter">
    <div class="layui-form-item" id="cid_hidde_model">
        <label class="layui-form-label">课程编号</label>
        <div class="layui-input-inline layui-input-wrap" style="width: 200px;">
            <input type="number" id="Cid" name="cid" maxlength="4" required placeholder="请输入课程编号"
                   autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-text-em" id="checkCId">长度：4位数字</div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">课程名称</label>
        <div class="layui-input-block">
            <input type="text" name="cname" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">课程类型</label>
        <div class="layui-input-block">
            <select name="type">
                <option value="课程类型">课程类型</option>
                <option value="必修">必修</option>
                <option value="选修">选修</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">学院</label>
        <div class="layui-input-block">
            <select name="belongcoll" id="belongcoll" lay-filter="aihao">
                <option value="请选择归属学院">请选择归属学院</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">专业</label>
        <div class="layui-input-block">
            <select name="belongpro" id="belongpro">
                <option value="请选择专业">请选择专业</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">说明</label>
        <div class="layui-input-block">
            <textarea placeholder="请输入课程说明" class="layui-textarea" name="cintroduction"></textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn" lay-submit lay-filter="demo-val">立即提交</button>
            <button type="reset" style="background-color:#66a2fe;" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/utils/layui/layui.js"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.$;
        var form = layui.form;

        $.get("/collegeServlet?flag=getColleges", function (result) {
            $.each(result, function (index, item) {
                console.log(11)
                const option = new Option(item.title, item.id);
                $('#belongcoll').append(option);
            });
            layui.form.render("select");
        }, "json");

        form.on('select(aihao)', function () {
            var collegeId = $("#belongcoll").val();
            console.log("进入二级菜单" + collegeId)
            $("#belongpro").empty();
            $.get("/majorServlet?flag=getMajors&collegeId=" + collegeId, function (result) {
                $.each(result, function (index, item) {
                    const option = new Option(item.mname, item.id);
                    $('#belongpro').append(option);
                });
                layui.form.render("select");
            }, "json");
        });

        var cinput = $("#Cid");
        cinput.blur(function () {
            cinput = $("#Cid");
            console.log("离开！")
            // 清理提示
            $('#checkCId').text("")
            console.log(cinput.val())
            // 课程编号校验
            $.get("/courseServlet?flag=chexckCid&cid=" + cinput.val(), function (result) {
                if (result === "true") {
                    $('#checkCId').text("课程编号已存在,请重新输入！")
                    $("#Cid").focus();//选中输入框
                    $('#checkCId').attr("style", "color:red!important;")
                } else if (cinput.val().length < 4 || cinput.val().length > 4) {
                    $('#checkCId').text("要求:长度为4位数字！")
                    $("#Cid").focus();//选中输入框
                    $('#checkCId').attr("style", "color:red!important;")
                } else {
                    $('#checkCId').text("课程编号可用")
                    $('#checkCId').attr("style", "color:#68c923!important;")
                }
            });
        });

        // 针对添加和编辑，仅以下代码进行区分
        //如果域对象中不存在回显数据，值默为添加操作
        var flag = "add";
        var modelData = localStorage.getItem('edit_data');
        if (modelData === "null" || modelData === "") {
            flag = "add";
        } else {
            // 是编辑操作
            // 隐藏课程id字段
            $('#cid_hidde_model').attr("style", "display:none;")
            flag = "edit";
            // 回显数据
            form.val("demo-val-filter", JSON.parse(modelData));
        }

        // 提交事件
        form.on('submit(demo-val)', function (data) {
            var field = data.field; // 获取表单字段值
            field.flag = flag;
            $.post("/courseServlet", field, function (result) {
                console.log(result)
                if (result) {
                    layer.msg("操作成功");
                    var index = parent.layer.getFrameIndex(window.name); // 先得到当前 iframe 层的索引
                    parent.layer.close(index); // 再执行关闭
                } else {
                    layer.msg("操作失败")
                }
            });
            return false; // 阻止默认 form 跳转
        });
    })
</script>

</body>
</html>