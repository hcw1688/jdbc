##### 基于Mybatis-Plus为思路，通过dbutils，C3p0封装构建高效的数据库访问工具类（持续更新中）
##### 实现思路基于Mybatis-Plus。配合一个简单的servlet项目做实战测试;
相关:设计模式，jdbcUtils，C3p0连接池，注解，反射，泛型，数据库连接池...

###  入门
```java
    1.定义Entity对象

    //定义数据库映射表名
    @Table("t_user")
    public class User {
        public User() {
        }
        /**
         * 定义主键字段，以及字段名
         * （如果仅标注为@Key，则使用该对象属性名设为数据库主键列名）
         */
        @Key(ColumnName = "Id")
        public Integer Id;
        ...
    }


    2. Dao继承BaseDao类
    
    /**
     * UserDao为自定义的接口        
     * (BaseDao中实现了一些通用的CRUD方法,详细代码请移步到DatabaseUtils类)
     */
    public class UserDaoImpl extends BaseDao<User> implements UserDao {
        @Override
        public List<User> list() {
            return this.listAll();
        }
    }
```

常用API
![img.png](doc/img/img2.png)


### 遇到的问题

1. 数据库字段与BeanListHandler映射

![img.png](doc/img/img.png)

### 参考规范 (来源于网络)

**命名规范**

tools & utils

tools 和 utils 都是常见的类名后缀，用于表示一个类是工具类或实用工具类。这两个后缀的使用并没有固定的规则。通常可以根据以下建议来决定应该使用哪个后缀:

1. 使用 tools 后缀:当一个工具类包含了多个方法，并且这些方法之间没有明显的关联时，可以使用 tools 后缀。例如，URLTools、StringTools 等。

2. 使用 utils 后缀:当一个工具类提供了一系列可重复使用的小型方法或函数，并且这些方法都属于相同的领域或目的时，可以使用 utils 后缀。例如，IOUtils、CollectionUtils 等。

------

### 测试

```sql
create database jdbc;
use jdbc;
/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : jdbc

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 10/09/2023 16:15:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
                           `Id` int(11) NOT NULL AUTO_INCREMENT,
                           `UserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                           `Password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                           `Email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                           `CreateTime` datetime(0) NULL DEFAULT NULL,
                           PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (2, 'Emma', 'password456', 'emma@example.com', '2023-09-22 14:05:24');
INSERT INTO `t_user` VALUES (3, 'Michael', 'password789', 'michael@example.com', '2023-09-22 14:05:28');
INSERT INTO `t_user` VALUES (4, 'ang', '123456', '635411106@qq.com', '2023-09-13 14:05:30');
INSERT INTO `t_user` VALUES (5, '牛牛不怕困难', '123456', '635411106@qq.com', '2023-09-20 14:05:42');
INSERT INTO `t_user` VALUES (6, '牛牛不怕困难', '123456', '635411106@qq.com', '2023-09-07 14:05:45');

SET FOREIGN_KEY_CHECKS = 1;

```
