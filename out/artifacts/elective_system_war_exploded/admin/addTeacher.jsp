<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="/utils/scripts/flat-ui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="/StudentInfo/utils/image/favicon.ico" type="image/x-icon"/>
    <meta charset="UTF-8">
    <title>学生信息管理系统</title>
    <script type="text/javascript" src="/utils/js/jquery-3.3.1.min.js"></script>
    <!-- Loading Bootstrap -->
    <link href="/utils/css/vendor/bootstrap.min.css" rel="stylesheet">
    <!-- Loading Flat UI Pro -->
    <link href="/utils/css/flat-ui.css" rel="stylesheet">
    <!-- Loading Flat UI JS -->
    <script type="text/javascript" src="/utils/scripts/flat-ui.min.js"></script>

    <script type='text/javascript' src='/utils/scripts/particles.js'></script>
    <link href="/utils/css/animate.css" rel="stylesheet">
</head>

<script>
    //页面加载完成后
    $(function () {
        var tid = $("#tid");
        var sub = $("#sub"); //提交按钮
        tid.blur(function () {
            var tidVal = tid.val();
            if (tidVal != "") { //tid的值不为空
                $.get("/teacherServlet?flag=checktid&tid=" + tidVal, function
                    (result) {
                    if (result == "true") { //教师编号已经存在
                        $("#counttid").text("教师编号已经存在")
                        $("#counttid").css("color", "red");
                        sub.prop("disabled", true);
                    } else {
                        $("#counttid").text("教师编号可用")
                        $("#counttid").css("color", "green");
                        sub.prop("disabled", false);
                    }
                });
            } else { // tid的值为 空串
                $("#counttid").text("教师编号不能为空")
                $("#counttid").css("color", "red");
                sub.prop("disabled", true);
            }
        });
    });
</script>

<body>
<div id="particles-js">
    <canvas class="particles-js-canvas-el" width="1322" height="774" style="width: 100%; height: 100%;"></canvas>
</div>
<jsp:include page="adminLeft.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h5>添加教师</h5>
            <form action="/teacherServlet?flag=add" method="post">
                <h6>教师编号</h6>

                <input type="text" name="tid" id="tid" class="form-control" oninput="value=value.replace(/[^\d]/g,'')"
                       maxlength="5"></input>
                <span id="counttid" style="color: #ff0000;"></span>
                <h6>姓名</h6>
                <input type="text" name="tname" class="form-control"
                       onkeyup="this.value=this.value.replace(/[^\u4e00-\u9fa5]/g,'')" maxlength="4"></input>
                <h6>密&nbsp;&nbsp;码</h6>
                <input type="password" name="tpassword" class="form-control"
                       onkeyup="this.value=this.value.replace(/(^\s+)|(\s+$)/g,'');" maxlength="16"></input>
                <h6>性别</h6>

                <label class="radio" for="radio1">
                    <input type="radio" name="tsex" checked value="男" id="radio1" data-toggle="radio"
                           class="custom-radio">
                    <span class="icons">
               				<span class="icon-unchecked"></span>
               				<span class="icon-checked"></span>
               			</span>
                    <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                            男
                        </font>
                    </font>
                </label>

                <label class="radio" for="radio2">
                    <input type="radio" name="tsex" value="女" id="radio2" data-toggle="radio" class="custom-radio">
                    <span class="icons">
               				<span class="icon-unchecked"></span>
               				<span class="icon-checked"></span>
               			</span>
                    <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                            女
                        </font>
                    </font>
                </label>

                <h6>个人简介</h6>
                <textarea name="introduction" cols="20" rows="6" class="form-control"></textarea>

                <input type="submit" value="添加" class="btn btn-primary btn-wide login-btn" style="margin-top:2rem"/>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="/utils/scripts/flat-ui.js">
</script>
<script src="/utils/scripts/bganimation.js"></script>
</body>
</html>